require("./bootstrap");

import { createApp } from "vue";
import store from "./store/web/index.js";
import signup from "./web/signup/Signup.vue";
import SocialMedia from "./web/signup/SocialMedia";
import UserProfile from "./web/signup/UserProfile";
import CustomerProfile from "./web/signup/CustomerProfile";

import LanguageModal from "./web/modals/LanguageModal.vue";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

createApp({})
    .use(VueSweetalert2)
    .component("signup", signup)

    .component("LanguageModal", LanguageModal)
    .component("SocialMedia", SocialMedia)
    .component("UserProfile", UserProfile)
    .component("CustomerProfile", CustomerProfile)
    .use(store)
    .mount("#canexp-app");
