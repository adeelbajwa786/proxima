import { createRouter, createWebHistory } from "vue-router";

import Dashboard from "../admin/Dashboard/Dashboard.vue";
import Languages from "../admin/Languages/Languages.vue";
import CreateLanguages from "../admin/Languages/Create.vue";
import BusinessCategories from "../admin/BusinessCategories/BusinessCategories.vue";
import CreateBusinessCategories from "../admin/BusinessCategories/Create.vue";
import RegistrationPackages from "../admin/RegistrationPackages/RegistrationPackages.vue";
import CreateRegistrationPackages from "../admin/RegistrationPackages/Create.vue";
import Errors from "../admin/Errors/Create.vue";
import RegPageSetting from "../admin/Pages/RegPageSetting.vue";
import LoginPageSetting from "../admin/Pages/LoginPageSetting.vue";
import HomePageSetting from "../admin/Pages/HomePageSetting.vue";

import CreatePages from "../admin/Pages/Create.vue";
import Pages from "../admin/Pages/Pages.vue";

import Profile from "../admin/Profile/Profile.vue";

import Degrees from "../admin/Degrees/Degrees.vue";
import CreateDegrees from "../admin/Degrees/Create.vue";

import Articles from "../admin/Articles/Articles.vue";
import CreateArticles from "../admin/Articles/Create.vue";

import ArticleCategories from "../admin/ArticleCategories/ArticleCategories.vue";
import CreateArticleCategories from "../admin/ArticleCategories/Create.vue";

import Menus from "../admin/Menus/Menus.vue";
import CreateMenus from "../admin/Menus/Create.vue";

const routes = [
    {
        path: "/dashboard",
        name: "admin.dashboard",
        component: Dashboard,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/languages",
        name: "admin.languages.index",
        component: Languages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Languages",
                    routeName: "admin.languages.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/languages/create",
        name: "admin.languages.create",
        component: CreateLanguages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Languages",
                    routeName: "admin.languages.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.languages.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/languages/:id/edit",
        name: "admin.languages.edit",
        component: CreateLanguages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Languages",
                    routeName: "admin.languages.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.languages.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/business-categories",
        name: "admin.business_categories.index",
        component: BusinessCategories,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Business categories",
                    routeName: "admin.business_categories.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/business-categories/create",
        name: "admin.business_categories.create",
        component: CreateBusinessCategories,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Business Categories",
                    routeName: "admin.business_categories.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.business_categories.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/business-categories/:id/edit",
        name: "admin.business_categories.edit",
        component: CreateBusinessCategories,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Business Categories",
                    routeName: "admin.business_categories.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.business_categories.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    //menus
    {
        path: "/menus",
        name: "admin.menus.index",
        component: Menus,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Menus",
                    routeName: "admin.menus.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/menus/create",
        name: "admin.menus.create",
        component: CreateMenus,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Menus",
                    routeName: "admin.menus.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.menus.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/menus/:id/edit",
        name: "admin.menus.edit",
        component: CreateMenus,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Menus",
                    routeName: "admin.menus.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.menus.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/degrees",
        name: "admin.degrees.index",
        component: Degrees,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Degrees",
                    routeName: "admin.degrees.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/degrees/create",
        name: "admin.degrees.create",
        component: CreateDegrees,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Degrees",
                    routeName: "admin.degrees.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.degrees.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/degrees/:id/edit",
        name: "admin.degrees.edit",
        component: CreateDegrees,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Degrees",
                    routeName: "admin.degrees.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.degrees.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/articles",
        name: "admin.articles.index",
        component: Articles,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Articles",
                    routeName: "admin.articles.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/articles/create",
        name: "admin.articles.create",
        component: CreateArticles,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Articles",
                    routeName: "admin.articles.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.articles.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/articles/:id/edit",
        name: "admin.articles.edit",
        component: CreateArticles,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Articles",
                    routeName: "admin.articles.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.articles.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/article_categories",
        name: "admin.article_categories.index",
        component: ArticleCategories,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Article Categories",
                    routeName: "admin.article_categories.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/article_categories/create",
        name: "admin.article_categories.create",
        component: CreateArticleCategories,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Article Categories",
                    routeName: "admin.article_categories.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.article_categories.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/article_categories/:id/edit",
        name: "admin.article_categories.edit",
        component: CreateArticleCategories,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Article Categories",
                    routeName: "admin.article_categories.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.article_categories.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/registration-packages",
        name: "admin.registration_packages.index",
        component: RegistrationPackages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Registration packages",
                    routeName: "admin.registration_packages.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/registration-packages/create",
        name: "admin.registration_packages.create",
        component: CreateRegistrationPackages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Registration packages",
                    routeName: "admin.registration_packages.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.registration_packages.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/registration-packages/:id/edit",
        name: "admin.registration_packages.edit",
        component: CreateRegistrationPackages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Registration packages",
                    routeName: "admin.registration_packages.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.registration_packages.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/profile",
        name: "admin.profile.edit",
        component: Profile,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Profile",
                    routeName: "admin.profile.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/errors",
        name: "admin.errors.edit",
        component: Errors,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Errors",
                    routeName: "admin.errors.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/pages/registration",
        name: "admin.registration.edit",
        component: RegPageSetting,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Registration page",
                    routeName: "admin.registration.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/pages/home",
        name: "admin.home.edit",
        component: HomePageSetting,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Homeistration page",
                    routeName: "admin.home.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/pages/login",
        name: "admin.login.edit",
        component: LoginPageSetting,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Login page",
                    routeName: "admin.login.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/pages",
        name: "admin.pages.index",
        component: Pages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Pages",
                    routeName: "admin.pages.index",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/pages/create",
        name: "admin.pages.create",
        component: CreatePages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Pages",
                    routeName: "admin.pages.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Create",
                    routeName: "admin.pages.create",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
    {
        path: "/pages/:id/edit",
        name: "admin.pages.edit",
        component: CreatePages,
        meta: {
            breadcrumbs: [
                {
                    name: "Dashboard",
                    routeName: "admin.dashboard",
                    isCurrentRoute: 0,
                },
                {
                    name: "Pages",
                    routeName: "admin.pages.index",
                    isCurrentRoute: 0,
                },
                {
                    name: "Edit",
                    routeName: "admin.pages.edit",
                    isCurrentRoute: 1,
                },
            ],
        },
    },
];

export default createRouter({
    history: createWebHistory(),
    routes,
});
