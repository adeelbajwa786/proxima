import { createStore } from "vuex";
import signup from "./signup.js";

export default new createStore({
    strict: true,
    modules: {
        signup,
    },
});
