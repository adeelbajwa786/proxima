import { createStore } from "vuex";
import menu from "./admin/menu.js";
import languages from "./admin/languages.js";
import business_categories from "./admin/business_categories.js";
import menus from "./admin/menus.js";
import registration_packages from "./admin/registration_packages.js";
import errors from "./admin/errors.js";
import reg_page_setting from "./admin/reg_page_setting.js";
import auth from "./admin/auth.js";
import degrees from "./admin/degrees.js";
import articles from "./admin/articles.js";
import article_categories from "./admin/article_categories.js";
import pages from "./admin/pages.js";
import login_page_setting from "./admin/login_page_setting.js";
import home_page_setting from "./admin/home_page_setting.js";
export default new createStore({
    strict: true,
    modules: {
        menu,
        languages,
        business_categories,
        registration_packages,
        auth,
        errors,
        reg_page_setting,
        degrees,
        articles,
        article_categories,
        pages,
        login_page_setting,
        menus,
        home_page_setting
    },
});
