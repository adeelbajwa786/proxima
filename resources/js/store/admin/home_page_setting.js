import ErrorHandling from "../../ErrorHandling";

const home_page_setting = {
    namespaced: true,
    state: {
        validationErros: new ErrorHandling(),
        error: null,
        form: {
            id: null,
            welcome_title: {},
            welcome_description: {},
            welcome_button_text: {},
            welcome_button_link: {},
            welcome_image: {},
            featured_title: {},
            featured_description: {},
            financial_title: {},
            financial_description: {},
            banner_1_title: {},
            banner_1_description: {},
            banner_1_button_text: {},
            banner_1_button_link: {},
            banner_1_image: {},
            banner_1_image_2: {},
            work_while_study_title: {},
            work_while_study_description: {},
            banner_2_title: {},
            banner_2_description: {},
            banner_2_button_text: {},
            banner_2_button_link: {},
            banner_2_image: {},
            banner_2_image_2: {},
            proxima_title: {},
            proxima_description: {},
            recent_article_title: {},
            recent_article_description: {},
            banner_3_title: {},
            banner_3_description: {},
            banner_3_button_text: {},
            banner_3_button_link: {},
            banner_3_image: {},
            banner_3_image_2: {},
        },
        home_page_setting: null,
        loading: false,
        limit: 10,
    },
    mutations: {
        setPageSetting(state, payload) {
            state.form[payload.key] = payload.value;
        },
        updatePageSetting(state, payload) {
            console.log(payload.key,`${payload.key}_${payload.id}`);
            state.form[payload.key][`${payload.key}_${payload.id}`] =
                payload.value;
        },
        setHomePageSetting(state, payload) {
            state.home_page_setting = payload;
        },
        setLoading(state, payload) {
            state.loading = payload ? payload : !state.loading;
        },
        resetForm(state) {
            state.form = {
                id: null,
                welcome_title: {},
                welcome_description: {},
                welcome_button_text: {},
                welcome_button_link: {},
                welcome_image: {},
                featured_title: {},
                featured_description: {},
                financial_title: {},
                financial_description: {},
                banner_1_title: {},
                banner_1_description: {},
                banner_1_button_text: {},
                banner_1_button_link: {},
                banner_1_image: {},
                banner_1_image_2: {},
                work_while_study_title: {},
                work_while_study_description: {},
                banner_2_title: {},
                banner_2_description: {},
                banner_2_button_text: {},
                banner_2_button_link: {},
                banner_2_image: {},
                banner_2_image_2: {},
                proxima_title: {},
                proxima_description: {},
                recent_article_title: {},
                recent_article_description: {},
                banner_3_title: {},
                banner_3_description: {},
                banner_3_button_text: {},
                banner_3_button_link: {},
                banner_3_image: {},
                banner_3_image_2: {},
            };
            state.validationErros = new ErrorHandling();
            state.error = null;
        },
        setForm(state, payload) {
            state.form.id = payload.id;
        },
        setValidationErros(state, payload) {
            state.validationErros.record(payload);
        },
        setEmptyError(state) {
            state.validationErros = new ErrorHandling();
        },
        setError(state, payload) {
            state.error = payload;
        },
    },
    actions: {
        addUpdateForm({ commit, state }) {
            let url = `${process.env.MIX_ADMIN_API_URL}home-page-setting`;
            commit("setLoading");
            return new Promise(function (resolve, reject) {
                axios
                    .post(url, state.form)
                    .then((res) => {
                        if (res.data.status == "Success") {
                            helper.swalSuccessMessage(res.data.message);
                            resolve(res);
                        } else {
                            helper.swalErrorMessage(res.data.message);
                        }
                    })
                    .catch((error) => {
                        commit("setEmptyError");
                        if (error.response && error.response.status == 422) {
                            commit(
                                "setValidationErros",
                                error.response.data.errors
                            );
                        } else if (
                            error.response &&
                            error.response.data &&
                            error.response.data.status == "Error"
                        ) {
                            helper.swalErrorMessage(
                                error.response.data.message
                            );
                        }
                        reject(error);
                    })
                    .finally(() => commit("setLoading"));
            });
        },
        fetchHomePageSetting({ commit, state }, payload) {
            let url = payload.url;
            commit("setLoading");
            return new Promise(function (resolve, reject) {
                axios
                    .get(url)
                    .then((res) => {
                        if (res.data.status == "Success") {
                            commit("setForm", res.data.data);
                            resolve(res);
                        }
                    })
                    .catch((error) => {
                        reject(error);
                    })
                    .finally(() => commit("setLoading"));
            });
        },
    },
};

export default home_page_setting;
