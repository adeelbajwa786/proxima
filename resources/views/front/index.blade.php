@extends('front.layouts.app')
@section('content')
    <div style="background-image: url('assets/banner.jpg');"
        class="backdrop-blur-sm bg-black bg-cover flex items-center justify-center h-96 w-full">
        <div class="w-11/12 xl:w-[30rem]">
            <div class="bg-white bg-opacity-40 p-2 rounded">
                <div class="flex items-center divide-x bg-white rounded ">
                    <input type="search" class="w-full p-2 px-4 focus:outline-none focus:ring-none"
                        placeholder="Search by school name">
                    <select class="bg-white text-gray-500 p-2 px-4" name="" id="">
                        <option value="">select 1</option>
                        <option value="">select 1</option>
                        <option value="">select 1</option>
                        <option value="">select 1</option>
                    </select>
                    <button class="bg-white p-2 px-4 text-gray-500">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                        </svg>
                    </button>
                </div>
            </div>
            <div class="bg-white bg-opacity-40 w-44 p-2 rounded mx-auto mt-6">
                <button class="bg-PrimaryRed px-4 whitespace-nowrap py-2 font-bold text-white">
                    Advanced Search
                </button>
            </div>
        </div>
    </div>

    <div class="bg-white w-full  border-b-2 border-PrimaryRed hidden lg:block">
        <div class="container mx-auto">
            <div class="grid xl:grid-cols-8 lg:grid-cols-4 md:grid-cols-4 sm:grid-cols-4 items-center py-4 lg:py-4">
                <div class="p-2 border-r">
                    <div class=""><img class="h-12 mx-auto" src="assets/1641016012_2948.png" alt=""></div>
                    <div class="mt-2 text-sm hover:font-bold font-semibold text-center white">
                        <p>Language Programs</p>
                    </div>
                </div>

                <div class="p-2 border-r">
                    <div><img class="h-8  mx-auto" src="assets/attachment.png" alt=""></div>
                    <div class="mt-4 text-sm hover:font-bold font-semibold text-center">
                        <p>Community College</p>
                    </div>
                </div>

                <div class="p-2 border-r">
                    <div><img class="h-10 mx-auto" src="assets/institute.png" alt=""></div>
                    <div class="mt-2 text-sm hover:font-bold font-semibold text-center">
                        <p>Bachelor Degree</p>
                    </div>
                </div>

                <div class="p-2 xl:border-r">
                    <div><img class="h-10 mx-auto" src="assets/degree.png" alt=""></div>
                    <div class="mt-2 text-sm hover:font-bold font-semibold text-center">
                        <p>Masters</p>
                    </div>
                </div>

                <div class="p-2 border-r">
                    <div><img class="h-8  mx-auto" src="assets/certificate.png" alt=""></div>
                    <div class="text-sm mt-2 hover:font-bold font-semibold text-center">
                        <p>Certificate/Short term</p>
                    </div>
                </div>
                <div class="p-2 border-r">
                    <div><img class="h-10 mx-auto" src="assets/summer.png" alt=""></div>
                    <div class="mt-2 text-sm hover:font-bold font-semibold text-center">
                        <p>Summer</p>
                    </div>
                </div>
                <div class="p-2 border-r">
                    <div><img class="h-10 mx-auto" src="assets/school.png" alt=""></div>
                    <div class="mt-2 text-sm hover:font-bold font-semibold text-center">
                        <p>High School</p>
                    </div>
                </div>
                <div class="p-2">
                    <div><img class="h-10 mx-auto" src="assets/online.png" alt=""></div>
                    <div class="mt-2 text-sm hover:font-bold font-semibold text-center transition-all">
                        <p>Online</p>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="bg-white">
        <div class="container mx-auto">


            <div class="bg-white py-6 lg:hidden">
                <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-2 gap-4">


                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div class=""><img class="md:h-10 h-10 mx-auto" src="assets/1641016012_2948.png"
                                    alt=""></div>
                            <div class=" text-sm font-semibold md:text-center">
                                <p>Language Programs</p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div><img class="md:h-9 h-7 mx-auto" src="assets/attachment.png" alt=""></div>
                            <div class="text-sm font-semibold md:text-center">
                                <p>Community College</p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div><img class="md:h-10 h-8 mx-auto" src="assets/institute.png" alt=""></div>
                            <div class="text-sm font-semibold md:text-center">
                                <p>Bachelor Degree</p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div><img class="md:h-10 h-8 mx-auto" src="assets/degree.png" alt=""></div>
                            <div class="text-sm font-semibold md:text-center">
                                <p>Masters</p>
                            </div>
                        </div>
                    </div>


                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div><img class="md:h-8 h-7  mx-auto" src="assets/certificate.png" alt=""></div>
                            <div class="text-sm font-semibold md:text-center">
                                <p>Certificate/Short term</p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div><img class="md:h-10 h-8 mx-auto" src="assets/summer.png" alt=""></div>
                            <div class="text-sm font-semibold md:text-center">
                                <p>Summer</p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div><img class="md:h-10 h-8 mx-auto" src="assets/school.png" alt=""></div>
                            <div class="text-sm font-semibold md:text-center">
                                <p>High School</p>
                            </div>
                        </div>
                    </div>

                    <div class="bg-gray-50 border rounded">
                        <div class="p-2 flex justify-center items-center gap-2">
                            <div><img class="md:h-10 h-7 mx-auto" src="assets/online.png" alt=""></div>
                            <div class="text-sm font-semibold md:text-center transition-all">
                                <p>Online</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>






        </div>

        <div class="bg-gray-50 ">
            <div class="grid grid-cols-2 container mx-auto">
                <div class="text-gray-600 py-14 pr-6">
                    <p class="text-xl font-semibold">Welcome to proxima study</p>
                    <p class="mt-2">Schools ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
                    <div class="mt-4">
                        <button class="bg-PrimaryRed text-sm font-semibold px-10 py-2 rounded-lg shadow text-white ">Find
                            out more</button>
                    </div>
                </div>
                <div class="">
                    <img class="mx-auto h-72" src="assets/banner.jpg" alt="">
                </div>
            </div>
        </div>

        <div class="container mx-auto mt-14">
            <div>
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Featured School</p>
                    <p class="">This is the "Featured schools" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4">
                        <div class="">
                            <div class=""><img class="h-44 object-fill w-full" src="assets/banner.jpg"
                                    alt=""></div>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">École secondaire Pierre-Laporte</p>
                            </div>
                        </div>

                        <div>
                            <div class=""> <img class="h-44 object-fill w-full" src="assets/palace.jpg"
                                    alt=""></div>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Vedha College</p>
                            </div>
                        </div>

                        <div>
                            <div class=""><img class="h-44 object-fill  w-full" src="assets/cardimage.jpg"
                                    alt=""></div>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Jamal College</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/buildings.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">College bois-de-boulogne</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/palace2.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Royal College</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Financial help for international students</p>
                    <p class="">This is the "Financial help for international students" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-56 w-56 mx-auto rounded-full object-fill" src="assets/car.jpg" alt="">
                            <div class="mt-2">
                                <p class="text-gray-600 truncate font-bold text-center">Test - Financial help for
                                    international students</p>
                                <p class="text-gray-500 text-sm text-center">Financial help for international students"
                                    description text</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-56 w-56 mx-auto rounded-full object-fill" src="assets/car.jpg" alt="">
                            <div class="mt-2">
                                <p class="text-gray-600 truncate font-bold text-center">Test - Financial help for
                                    international students</p>
                                <p class="text-gray-500 text-sm text-center">Financial help for international students"
                                    description text</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-56 w-56 mx-auto rounded-full object-fill" src="assets/car.jpg" alt="">
                            <div class="mt-2">
                                <p class="text-gray-600 truncate font-bold text-center">Test - Financial help for
                                    international students</p>
                                <p class="text-gray-500 text-sm text-center">Financial help for international students"
                                    description text</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-56 w-56 mx-auto rounded-full object-fill" src="assets/car.jpg" alt="">
                            <div class="mt-2">
                                <p class="text-gray-600 truncate font-bold text-center">Test - Financial help for
                                    international students</p>
                                <p class="text-gray-500 text-sm text-center">Financial help for international students"
                                    description text</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="mt-14">
                <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 rounded-md">
                    <div style="background-image: url('assets/banner2\ \(1\).png')"
                        class="bg-cover bg-no-repeat h-60 w-full md:rounded-l-lg flex items-center px-6">
                        <div>
                            <p class="text-2xl font-bold text-white">Imigration Questions</p>
                            <p class="text-white">Schools ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                tempor incididunt</p>
                            <button class="bg-white px-6 py-1.5 text-black font-semibold shadow rounded-md mt-3">Find out
                                more</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-fill h-60 md:rounded-r-lg" src="assets/banner.jpg" alt="">
                    </div>
                </div>
            </div>


            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Financial help for Canadian students</p>
                    <p class="">This is the "Financial help for Canadian students" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/car.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Financial help for
                                    international students</p>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <!--Slider section-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Work while studying</p>
                    <p class="">This is the "Work while studying" description text</p>
                </div>
                <div class="w-full h-60 rounded-md border-4 border-black my-4">
                    <!-- Swiper -->
                    <div class="swiper mySwiper ">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/career beacon.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>career beacon</p>
                                    </div>
                                </div>

                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/GOOGLE.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Google</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/i logo.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Indeed</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/jublee co.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Jobillico</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/job posting.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>JopPosting</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-10 m-auto"
                                            src="assets/linkedin.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Linkedin</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets//magnet.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Magnet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-10 m-auto"
                                            src="assets/monster.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Monster</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-10 m-auto"
                                            src="assets/neuvoo.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Neuvoo</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/jobbank.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Job Bank</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/talentegg.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>TalentEgg</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/ziprecuiter.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>Zip Recuiter</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/ag careers.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>AgCareers</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-6 left-0 bottom-0 h-14 m-auto"
                                            src="assets/careersinfood.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>CareersInFood.com</p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div>
                                    <div class="p-4 relative">
                                        <img class="h-24" src="assets/logo_frame.png" alt="">
                                        <img class="absolute top-0 right-0 left-0 bottom-0 h-12 m-auto"
                                            src="assets/civicjobs.png" alt="">
                                    </div>
                                    <div class="text-center text-gray-500">
                                        <p>CivicJobs.ca</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="swiper-button-next">
                            <div class="flex justify-center items-center rounded-full p-1 text-white bg-PrimaryRed">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                    stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                </svg>

                            </div>
                        </div>
                        <div class="swiper-button-prev">
                            <div class="flex justify-center items-center rounded-full p-1 text-white bg-PrimaryRed">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                    stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                        d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75" />
                                </svg>


                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!--immigration questions-->
            <div class="mt-14">
                <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 rounded-md">
                    <div style="background-image: url('assets')"
                        class="bg-cover bg-no-repeat h-60 w-full md:rounded-l-lg flex items-center px-6">
                        <div>
                            <p class="text-2xl font-bold text-white">Imigration Questions</p>
                            <p class="text-white">Schools ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                tempor incididunt</p>
                            <button class="bg-white px-6 py-1.5 text-black font-semibold shadow rounded-md mt-3">Find out
                                more</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-fill h-60 md:rounded-r-lg" src="assets/immigrationbg.png" alt="">
                    </div>
                </div>
            </div>

            <!--Financial Planning-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Financial planning</p>
                    <p class="">This is the "Financial planning" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/car.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/car.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Financial Planning</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <!--academic help-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">For students who need academic help before applying</p>
                    <p class="">This is the "For students who need academic help before applying" description text
                    </p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/car.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Financial Planning</p>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <!--Fianancial help-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">For students who need financial help before applying</p>
                    <p class="">This is the "For students who need financial help before applying" description text
                    </p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/car.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Financial Planning</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!--Immigration Questions-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Immigration questions</p>
                    <p class="">This is the "Immigration questions" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div class="shadow">
                            <div class="p-4"><img class="max-w-[166px] mx-auto" src="assets/find.png" alt="">
                            </div>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Immigration questions</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!--Proxima study-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Proxima Study is coming to you</p>
                    <p class="">This is the "Proxima Study in coming to you" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div class="shadow">
                            <div class="p-4"><img class="max-w-[166px] mx-auto" src="assets/find.png" alt="">
                            </div>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Proxima study coming to you</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!--Need more help-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Need more help?</p>
                    <p class="">This is the "Need more help?" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/car.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Proxima study coming to you</p>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <!--Open days-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Open days</p>
                    <p class="">This is the "Featured open days" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Interview</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays2.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - open day</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--Virual Tours-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Virtual tours</p>
                    <p class="">This is the "Virtual tours" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div class="relative">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Jamal University</p>
                            </div>
                            <img class="h-48 w-full" src="assets/opendays1.jpg" alt="">
                            <div class="text-white absolute bottom-2 p-2 right-0 left-0 mx-auto">
                                <p class="text-center">We are in Sydney, Western, Australia</p>
                            </div>
                        </div>

                        <div class="relative">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Huron University College</p>
                            </div>
                            <img class="h-48 w-full" src="assets/opendays2.jpg" alt="">
                            <div class="text-white absolute bottom-2 p-2 right-0 left-0 mx-auto">
                                <p class="text-center">We are in Pottuvil, Eastern, Sri Lanka</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--Open days-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Featured businesses</p>
                    <p class="">This is the "Featured businesses" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Engineer</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays2.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Delivery Boy</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays2.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Telecom Engineer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <!--canadian education-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Getting started with your Canadian education</p>
                    <p class="">This is the "Getting started with your Canadian education" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Immigration questions</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays2.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Work while study</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays2.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Proxima study coming to you</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--Featured Events-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Featured Events</p>
                    <p class="">This is the "Featured events" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Save trees</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Save water</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



            <!--student services-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Student services</p>
                    <p class="">This is the "Student services" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Real Estate Broker</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Phone Repairing</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Gym Coach</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Engineer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!--Featured Videos-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Featured videos</p>
                    <p class="">This is the "Featured videos" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <iframe class="h-44 object-fill w-full" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                            </iframe>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Canada in winter. Winter in Canada</p>
                            </div>
                        </div>

                        <div>
                            <iframe class="h-44 object-fill w-full" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                            </iframe>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Student loans for international
                                    students in Canada</p>
                            </div>
                        </div>

                        <div>
                            <iframe class="h-44 object-fill w-full" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                            </iframe>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Scholarships for international
                                    students in Canada</p>
                            </div>
                        </div>

                        <div>
                            <iframe class="h-44 object-fill w-full" src="https://www.youtube.com/embed/tgbNymZ7vqY">
                            </iframe>
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Do you want to study in Canada?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--Recent articles-->
            <div class="mt-14">
                <div class="text-gray-600">
                    <p class="text-xl font-bold">Recent articles</p>
                    <p class="">This is the "Recent articles" description text</p>
                </div>
                <div>

                    <div class="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4 my-4 mt-4">
                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Need more help</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Proxima study coming to you</p>
                            </div>
                        </div>

                        <div>
                            <img class="h-44 object-fill w-full" src="assets/opendays1.jpg" alt="">
                            <div class="bg-gradient-to-r from-black to-PrimaryRed py-3 px-4">
                                <p class="text-white truncate font-bold text-center">Test - Immigration questions</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="mt-14">
                <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 rounded-md">
                    <div style="background-image: url('assets/divbanner.jpg')"
                        class="bg-cover bg-no-repeat h-60 w-full md:rounded-l-lg flex items-center px-6">
                        <div>
                            <p class="text-2xl font-bold text-white">Imigration Questions</p>
                            <p class="text-white">Schools ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                tempor incididunt</p>
                            <button class="bg-white px-6 py-1.5 text-black font-semibold shadow rounded-md mt-3">Find out
                                more</button>
                        </div>
                    </div>
                    <div>
                        <img class="object-fill h-60 md:rounded-r-lg" src="assets/banner.jpg" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
