<!DOCTYPE html>
<html lang="en" class="light scroll-smooth" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Proxima Study</title>
    <link href="/dist/output.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />

    <style>
        .swiper {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            object-fit: cover;
        }

        .swiper-button-next::after,
        .swiper-button-prev::after {
            content: "";
        }
    </style>
</head>

<body>
    <div class="sticky top-0 z-50">
        <div class="bg-darkgray py-4 w-full">
            <div class="container mx-auto flex items-center justify-center lg:justify-end divide-x">
                <button class="text-white text-sm font-semibold px-4">Register a School</button>
                <button class="text-white text-sm font-semibold px-4">Register a Buisness</button>
                <button class="text-white text-sm font-semibold px-4">Login</button>
            </div>
        </div>




        <nav class="py-2 bg-PrimaryRed relative w-full shadow-lg">
            <div class="flex justify-between items-center container mx-auto">
                <div><a href="#"><img class="max-w-[150px]" src="assets/1651430368_7424.png" alt=""></a>
                </div>
                <div class="absolute bottom-0.5 right-0 left-0 mx-auto max-w-[80px] hidden lg:block"><img class=""
                        src="assets/logo.png" alt=""></div>
                <div class="hidden lg:block">
                    <ul class="inline-flex text-white items-center text-sm font-bold divide-x-2">
                        <li class="px-3"><a href="#"> Schools</a></li>
                        <li class="px-3"><a href="#"> Scholarships</a></li>
                        <li class="px-3"><a href="#"> Buisnesses</a></li>
                        <li class="px-3"><a href="#"> Careers</a></li>
                        <li class="px-3"><a href="#"> Jobs</a></li>
                    </ul>
                </div>
                <div class="lg:hidden">
                    <a href="#" onclick="responsivenav()">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2"
                            stroke="currentColor" class="w-6 h-6 text-gray-100">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                d="M3.75 6.75h16.5M3.75 12h16.5M12 17.25h8.25" />
                        </svg>
                    </a>
                    <div id="responsivenav" class="absolute left-0 top-[75px] py-10 bg-PrimaryRed w-full hidden">
                        <ul class=" text-white text-center flex-col justify-center items-center space-y-2 font-bold">
                            <li class="px-3"><a href="#"> Schools</a></li>
                            <li class="px-3"><a href="#"> Scholarships</a></li>
                            <li class="px-3"><a href="#"> Buisnesses</a></li>
                            <li class="px-3"><a href="#"> Careers</a></li>
                            <li class="px-3"><a href="#"> Jobs</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    @yield('content')
    <footer class="bg-PrimaryRed w-full mt-14 py-14">
        <div class="container mx-auto">
            <div class="grid lg:grid-cols-3 md:grid-cols-3 sm:grid-cols-1 gap-4">
                <div>
                    <div class="bg-gray-300 p-2 rounded font-bold text-center">
                        <p>Proxima Study</p>
                    </div>
                    <div class="lg:text-center text-gray-100 my-6">
                        <ul class="space-y-1">
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Comments / suggestions</a></li>
                            <li><a href="#">Cookies policy</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">Disclaimer</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Meet our team</a></li>
                            <li><a href="#">Our sponsors</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Terms of use</a></li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="bg-gray-300 p-2 rounded font-bold text-center">
                        <p>Resources</p>
                    </div>
                    <div class="lg:text-center text-gray-100 my-6">
                        <ul class="space-y-1">
                            <li><a href="#">Articles</a></li>
                            <li><a href="#">Events</a></li>
                            <li><a href="#">Master application</a></li>
                            <li><a href="#">Online business directory</a></li>
                            <li><a href="#">Open days</a></li>
                            <li><a href="#">Programs</a></li>
                            <li><a href="#">Quotes</a></li>
                            <li><a href="#">Videos</a></li>
                            <li><a href="#">Virtual tours</a></li>
                            <li><a href="#">Worldwide networks</a></li>
                        </ul>
                    </div>
                </div>
                <div>
                    <div class="bg-gray-300 p-2 rounded font-bold text-center">
                        <p>More links</p>
                    </div>
                    <div class="lg:text-center text-gray-100 my-6">
                        <ul class="space-y-1">
                            <li><a href="#">Site map</a></li>
                            <li><a href="#">Stories</a></li>
                            <li><a href="#">link-3</a></li>
                            <li><a href="#">link-4</a></li>
                            <li><a href="#">link-5</a></li>
                            <li><a href="#">link-6</a></li>
                            <li><a href="#">link-7</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div class="flex justify-center items-center gap-2">
                    <div
                        class="bg-[#830b0b] group transition-all text-white hover:bg-[#4267B2] rounded-full h-10 w-10 flex justify-center items-center">
                        <img class="h-4" src="assets/Icon awesome-facebook-f.png" alt="">
                    </div>
                    <div
                        class="bg-[#830b0b] group transition-all text-white hover:bg-[#0072b1] rounded-full h-10 w-10 flex justify-center items-center">
                        <img class="h-4" src="assets/Icon awesome-linkedin-in.png" alt="">
                    </div>
                    <div
                        class="bg-[#830b0b] group transition-all text-white hover:bg-gradient-to-r from-[#962fbf] via-[#d62976] to-[#fa7e1e] rounded-full h-10 w-10 flex justify-center items-center">
                        <img class="h-5" src="assets/Icon feather-instagram.png" alt="">
                    </div>
                    <div
                        class="bg-[#830b0b] group transition-all text-white hover:bg-[#FF0000] rounded-full h-10 w-10 flex justify-center items-center">
                        <img class="h-4" src="assets/Icon simple-youtube.png" alt="">
                    </div>
                    <div
                        class="bg-[#830b0b] group transition-all text-white hover:bg-[#1DA1F2] rounded-full h-10 w-10 flex justify-center items-center">
                        <img class="h-4" src="assets/Icon awesome-twitter.png" alt="">
                    </div>
                </div>
            </div>
            <div class="text-sm text-center text-white mt-4">
                <p>Copyright © 2006 - 2022, 4R Business Services Inc. All rights reserved</p>
            </div>
        </div>
    </footer>

    <!-- Swiper JS -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            slidesPerView: 3,
            spaceBetween: 30,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                "@0.00": {
                    slidesPerView: 1,
                    spaceBetween: 10,
                },
                "@0.75": {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                "@1.00": {
                    slidesPerView: 3,
                    spaceBetween: 40,
                },
                "@1.50": {
                    slidesPerView: 4,
                    spaceBetween: 50,
                },
            },
        });
    </script>
    <script>
        function responsivenav() {
            var x = document.getElementById("responsivenav");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
</body>

</html>
