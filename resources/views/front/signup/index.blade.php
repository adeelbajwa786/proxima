@extends('front.layouts.app')
@section('title', 'Canadian Exports | Register your account')
@section('meta_description', 'Canadian Exports is a Canadian export portal and a directory of Canadian exporters, showcasing lists of Canadian products and services, and promoting Canadian manufacturers and exporters. Canadian Exports is a Canadian business directory and a Canadian business database highlighting the Canadian industry')
@section('Canadian Export, Export from Canada, Canada export, Export Canada, Exporting from Canada, Canada export catalogue, Canada export directory, Directory of, Canadian exporters, Canada business directory, Directory of Canadian companies, Directory of Canadian companies, Canada trade, Canadian trade, Canada export portal, Canada trade mission')
@section('content')
<section class="relative md:py-16 py-8">

    <div class="container md:mt-8 mt-4 pt-4">
        <signup></signup>
    </div>
  </section>
@endsection