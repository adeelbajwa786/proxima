<header>
    <nav class="bg-gray-50 text-gray-800 shadow-md h-14 md:h-auto border-b border-gray-700 relative z-20">
        <div class="mx-auto px-4 py-2 lg:px-12 desktop:px-80">
            <div class="flex justify-between items-center">
                <div class="flex space-x-4">
                    <div>
                        <!-- Website Logo -->
                        <a href="{{ route('front.index') }}" class="flex items-center">
                            <img src="{{ asset('assets/image/logo-dark.png') }}" alt="Logo" class="w-32 lg:w-48">
                        </a>
                    </div>
                </div>
                <!-- Secondary Navbar items -->
                <div class="hidden md:flex items-center md:space-x-0 lg:space-x-3 ">
                    <!-- Primary Navbar items -->
                    <div class="flex md:space-x-1 md:text-sm lg:text-base lg:space-x-3">
                        <div class="flex flex-wrap">
                            <div class="w-full px-4">
                                <div class="relative inline-flex align-middle w-full">
                                    <button type="button"
                                        class="menu font-FuturaBold text-sm lg:text-base text-gray-800  hover:text-primaryRed font-medium transition duration-300 p-1 lg:p-2"
                                        onclick="openDropdown(event,'dropdown-id')">
                                        About us
                                    </button>
                                    <div class="hidden bg-white divide-y z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1"
                                        style="min-width:12rem" id="dropdown-id">
                                        <a href="#"
                                            class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                            About us
                                        </a>
                                        <a href="#"
                                            class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                            How the business categories came about
                                        </a>
                                        <a href="#"
                                            class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                            Inquiries to buy
                                        </a>
                                        <a href="#"
                                            class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                            How do we delete our profile
                                        </a>
                                        <a href="#"
                                            class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                            Scam alerts
                                        </a>
                                        <a href="#"
                                            class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                            Success stories
                                        </a>
                                        <a href="#"
                                            class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                            Contact us
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#"
                            class="menu font-FuturaBold text-sm lg:text-base text-gray-800  hover:text-primaryRed font-medium transition duration-300 p-1 lg:p-2">
                            Career</a>
                        <a href="#"
                            class="menu font-FuturaBold text-sm lg:text-base text-gray-800  hover:text-primaryRed font-medium transition duration-300 p-1 lg:p-2">
                            Production
                        </a>
                        <a href="#"
                            class="menu font-FuturaBold text-sm lg:text-base text-gray-800  hover:text-primaryRed font-medium transition duration-300 p-1 lg:p-2">
                            Annoucement
                        </a>
                        <a href="#"
                            class="menu font-FuturaBold text-sm lg:text-base text-gray-800  hover:text-primaryRed font-medium transition duration-300 p-1 lg:p-2">
                            Contact
                        </a>
                    </div>
                </div>
                <div class="hidden md:flex items-center space-x-2">
                    @auth('customers')
                    <div class="flex flex-wrap">
                        <div class="w-full px-4">
                            <div class="relative inline-flex align-middle w-full">
                                <button type="button"
                                    class="menu font-FuturaBold text-sm lg:text-base text-gray-800  hover:text-primaryRed font-medium transition duration-300 p-1 lg:p-2"
                                    onclick="openDropdown(event,'dropdown-id2')">
                                    Welcome {{ auth()->guard('customers')->user()->name }} <i class="fa fa-user"></i>
                                </button>
                                <div class="hidden bg-white divide-y z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1"
                                    style="min-width:12rem" id="dropdown-id2">
                                    <a href="{{route('user.account-settings.index')}}"
                                        class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                        Account Settings
                                    </a>
                                    <a href="{{route('user.buissness-settings.index')}}"
                                        class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                       Business profile setting
                                    </a>
                                    <a href="#"
                                        class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                        Media Setting
                                    </a>
                                    <a href="{{route('user.social-media-settings.index')}}" 
                                    href="#"
                                        class="menu font-FuturaBold text-sm lg:text-base py-2 px-4 block w-full whitespace-nowrap bg-transparent text-slate-700">
                                       Social Media Setting
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                        <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-2"
                            href="#"
                            onclick="event.preventDefault(); document.getElementById('user-logout-form').submit();">
                            Logout
                        </a>
                        <form id="user-logout-form" action="{{ route('web.user.logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    @endauth
                    @guest('customers')
                        <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-2"
                        href="{{ route('web.signin') }}">
                            Log In
                        </a>
                        <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-2"
                            href="{{ route('web.signup') }}">
                            Register
                        </a>
                    @endguest
                </div>
                <!-- Mobile menu button -->
                <div class="md:hidden flex items-center">
                    <button class="outline-none mobile-menu-button">
                        <svg class=" w-6 h-6 text-gray-500 hover:text-primaryRed " x-show="!showMenu" fill="none"
                            stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path d="M4 6h16M4 12h16M4 18h16"></path>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <!-- mobile menu -->
        <div class="hidden mobile-menu bg-gray-50 h-screen text-gray-800 relative z-20">
            <ul class="">
                <li class="active"><a href="{{ route('front.index') }}"
                        class="block text-sm p-4 text-white bg-primaryRed font-semibold">Home</a></li>
                <li><a href="#"
                        class="block text-sm p-4 hover:bg-primaryRed transition duration-300 p-1 lg:p-2">About Us</a>
                </li>
                <li><a href="#"
                        class="block text-sm p-4 hover:bg-primaryRed transition duration-300 p-1 lg:p-2">Career</a>
                </li>
                <li><a href="#"
                        class="block text-sm p-4 hover:bg-primaryRed transition duration-300 p-1 lg:p-2">Production</a>
                </li>
                <li><a href="#"
                        class="block text-sm p-4 hover:bg-primaryRed transition duration-300 p-1 lg:p-2">Annoucement</a>
                </li>
                <li><a href="#"
                        class="block text-sm p-4 hover:bg-primaryRed transition duration-300 p-1 lg:p-2">Contact</a>
                </li>
            </ul>
            <div class="space-y-2 p-4">
                <button
                    class="text-primaryRed w-full md:w-auto border border-primaryRed hover:bg-primaryRed hover:text-white  px-4 py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150"
                    type="button">
                    Log In
                </button>
                <a class="text-primaryRed w-full md:w-auto border border-primaryRed hover:bg-primaryRed hover:text-white  px-4 py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150"
                    href="{{ route('web.signup') }}">
                    Register
                </a>
            </div>
        </div>
    </nav>
</header>
