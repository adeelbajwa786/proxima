<footer class="bg-primaryDark">
    <div class=" px-4 py-6 md:p-12 desktop:px-80">
        <div class="flex flex-col lg:flex-row">
            <div class="lg:w-[30%]">
                <img src="{{asset('assets/image/logo-white.png')}}" alt="">
                <p class="text-white mt-6 w-3/4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <ul class="flex justify-center md:justify-start items-center space-x-4 md:mt-6 md:mb-12 lg:mb-0 lg:mt-14">
                    <li class="bg-white border border-white text-primaryRed hover:border-primaryRed hover:text-white hover:bg-primaryRed rounded-full flex w-10 h-10 items-center justify-center">
                        <a href="/" rel="noreferrer" target="_blank">
                            <span class="sr-only">Facebook</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="10.252" height="21.999" fill="currentColor" viewBox="0 0 10.252 21.999">
                                <g transform="translate(-125.619)">
                                    <path class="a" d="M132.441,7.208v-1.9a4.176,4.176,0,0,1,.04-.661,1.283,1.283,0,0,1,.18-.461.736.736,0,0,1,.454-.314,3.231,3.231,0,0,1,.835-.087h1.9V0h-3.03a4.932,4.932,0,0,0-3.778,1.248,5.257,5.257,0,0,0-1.148,3.678V7.208h-2.27V11h2.269V22h4.552V11h3.03l.4-3.791Z" transform="translate(0)" />
                                </g>
                            </svg>
                        </a>
                    </li>

                    <li class="bg-white border border-white text-primaryRed hover:border-primaryRed hover:text-white hover:bg-primaryRed rounded-full flex w-10 h-10 items-center justify-center">
                        <a href="/" rel="noreferrer" target="_blank">
                            <span class="sr-only">Twitter</span>
                            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
                                <path d="M8.29 20.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0022 5.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.072 4.072 0 012.8 9.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 012 18.407a11.616 11.616 0 006.29 1.84" />
                            </svg>
                        </a>
                    </li>

                    <li class="bg-white border border-white text-primaryRed hover:border-primaryRed hover:text-white hover:bg-primaryRed rounded-full flex w-10 h-10 items-center justify-center">
                        <a href="/" rel="noreferrer" target="_blank">
                            <span class="sr-only">
                                linkedIn
                            </span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16.968" height="17.199" fill="currentColor" viewBox="0 0 16.968 17.199">
                                <g transform="translate(0 -9.558)">
                                    <path class="a" d="M16.968,20.1v6.654H13.331V20.548c0-1.56-.526-2.624-1.842-2.624a2.006,2.006,0,0,0-1.866,1.411,2.789,2.789,0,0,0-.121.941v6.481H5.864s.049-10.515,0-11.6H9.5V16.8c-.007.012-.017.026-.024.037H9.5V16.8a3.586,3.586,0,0,1,3.278-1.918C15.173,14.879,16.968,16.538,16.968,20.1ZM2.059,9.558a1.949,1.949,0,0,0-2.059,2,1.938,1.938,0,0,0,2.01,2.006h.024a1.95,1.95,0,0,0,2.058-2.006A1.942,1.942,0,0,0,2.059,9.558ZM.216,26.757H3.853v-11.6H.216Z" transform="translate(0)" />
                                </g>
                            </svg>
                        </a>
                    </li>

                    <li class="bg-white border border-white text-primaryRed hover:border-primaryRed hover:text-white hover:bg-primaryRed rounded-full flex w-10 h-10 items-center justify-center">
                        <a href="/" rel="noreferrer" target="_blank">
                            <span class="sr-only">
                                call
                            </span>
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="currentColor" viewBox="0 0 512 512">
                                <path d="M391 480c-19.52 0-46.94-7.06-88-30-49.93-28-88.55-53.85-138.21-103.38C116.91 298.77 93.61 267.79 61 208.45c-36.84-67-30.56-102.12-23.54-117.13C45.82 73.38 58.16 62.65 74.11 52a176.3 176.3 0 0128.64-15.2c1-.43 1.93-.84 2.76-1.21 4.95-2.23 12.45-5.6 21.95-2 6.34 2.38 12 7.25 20.86 16 18.17 17.92 43 57.83 52.16 77.43 6.15 13.21 10.22 21.93 10.23 31.71 0 11.45-5.76 20.28-12.75 29.81-1.31 1.79-2.61 3.5-3.87 5.16-7.61 10-9.28 12.89-8.18 18.05 2.23 10.37 18.86 41.24 46.19 68.51s57.31 42.85 67.72 45.07c5.38 1.15 8.33-.59 18.65-8.47 1.48-1.13 3-2.3 4.59-3.47 10.66-7.93 19.08-13.54 30.26-13.54h.06c9.73 0 18.06 4.22 31.86 11.18 18 9.08 59.11 33.59 77.14 51.78 8.77 8.84 13.66 14.48 16.05 20.81 3.6 9.53.21 17-2 22-.37.83-.78 1.74-1.21 2.75a176.49 176.49 0 01-15.29 28.58c-10.63 15.9-21.4 28.21-39.38 36.58A67.42 67.42 0 01391 480z" />
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="lg:w-[70%]">
                <div class="grid grid-cols-1 md:grid-cols-3 gap-6">
                    <!--Link-->
                    <div>
                        <h6 class="font-FuturaBold text-white text-2xl"> Useful links</h6>
                        <nav aria-label="Footer Navigation - Company" class="mt-4">
                            <ul class="space-y-2 font-medium">
                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Home
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        About us
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        How the business categories came
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        About
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Inquiries to buy
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        One more thing
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Scam alerts
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Testimonials
                                    </a>
                                </li>


                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Info-letter
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Success stories
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Resources
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!--To the Canadian Exporter-->
                    <div>
                        <h6 class="font-FuturaBold text-white text-2xl">To the Canadian Exporter</h6>
                        <nav aria-label="Footer Navigation - Company" class="mt-4">
                            <ul class="space-y-2 font-medium">
                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Circulation
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        FAQ (frequently asked questions)
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Rates & information
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Register my business
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Satisfaction guarantee
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Our business
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!--Other links-->
                    <div>
                        <h6 class="font-FuturaBold text-white text-2xl">Other links</h6>
                        <nav aria-label="Footer Navigation - Company" class="mt-4">
                            <ul class="space-y-2 font-medium">
                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Disclaimer
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Contact us
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Privacy policy
                                    </a>
                                </li>

                                <li>
                                    <a href="#" class="footer-menu font-FuturaBold text-sm lg:text-base hover:text-primaryRed font-medium transition duration-300 text-gray-100 transition hover:text-gray-400 flex items-center space-x-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                        </svg>
                                        Comments / suggestions
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="px-4 py-6 md:px-12 desktop:px-80 items-center mt-10 text-white border-t">
        <p>Copyright © 2006 - 2023. All right reserved. 4R Business Services Inc . </p>
    </div>
</footer>