<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @if (Auth::check()) 
    <meta name="user" content="{{ Auth::user() }}">
    @endif 
    <title>Dashboard</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <script src="{{asset('js/app.js')}}" defer></script>
</head>

<body class="h-screen">
    
    <div id="app">
        <router-view />
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
</body>

</html>