<x-web-layout>
    <x-slot:title>
        Complete your pending payment | Canadian Export
        </x-slot>
        <section class="bg-white px-4 py-6 md:p-12 desktop:px-80">
            <h1 class="font-bold text-center text-primaryRed text-4xl lg:text-5xl mb-12">Review & confirm</h1>
            <p class="lg:text-lg mb-4 lg:mb-8">
                Please take the time to review your subscription details and make any changes as necessary. Once you have checked that all your details are correct, please select proceed. You will then be directed to payment
            </p>
            <!--Form-->
            <form>
                <div class="grid grid-cols-1 lg:grid-cols-2 gap-6 xl:gap-12 items-center">
                    <div>
                        <div class="py-4 space-y-2">
                            <h4 class="text-primaryRed font-semibold text-lg">Registration package:</h4>
                            <p>Three months subscription -</p>
                            <p>$13.98 (One month free)</p>
                        </div>

                        <div class="py-4 space-y-3">
                            <h4 class="text-primaryRed font-semibold text-lg">User profile</h4>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="full-name">
                                        Your name and title:
                                    </label>
                                </div>
                                <div class="md:w-1/2">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full" id="full-name" type="text" value="" />
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="email">
                                        Your email:
                                    </label>
                                </div>
                                <div class="md:w-1/2">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full" placeholder="upload@claimbuyout.com" id="email" type="email"
                                        value="" />
                                </div>
                            </div>
                        </div>

                        <div class="py-4 space-y-2">
                            <h4 class="text-primaryRed font-semibold text-lg">Selected business categories (Industry sectors):</h4>
                            <p>Automotive & Aviation </p>
                            <p>Agriculture, Aquaculture & Livestock</p>
                        </div>

                        <div class="py-4 space-y-3">
                            <h4 class="text-primaryRed font-semibold text-lg">User Profile</h4>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4"
                                        for="company-name">
                                        Company Name:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        placeholder="test" id="company-name" type="text" value="">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4"
                                        for="company-email">
                                        Company Email:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        placeholder="upload@claimbuyout.com" id="company-email" type="email"
                                        value="">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4"
                                        for="mailing-address">
                                        Mailing Address:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <textarea rows="4"
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                          focus:outline-none focus:ring w-full"
                                        placeholder="A eiusmod qui maxime "></textarea>
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4"
                                        for="mailing-address">
                                        Detailed Business Description:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <textarea rows="4"
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                          focus:outline-none focus:ring w-full"
                                        placeholder="Detailed business description * "></textarea>
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="phone">
                                        Phone:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        id="phone" type="number" value="" placeholder="2123131321">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="website">
                                        Website:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        id="website" type="url" value=""
                                        placeholder="http://canexp.test/register">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="facebook">
                                        Facebook:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        placeholder="" id="facebook" type="url" value="">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="twitter">
                                        Twitter:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        placeholder="" id="twitter" type="url" value="">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="youtube">
                                        Youtube:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        placeholder="" id="youtube" type="url" value="">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4" for="linkedin">
                                        LinkedIn:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        placeholder="test" id="linkedin" type="url" value="">
                                </div>
                            </div>
                            <div class="md:flex md:items-start mb-6 md:ml-4">
                                <div class="md:w-1/3">
                                    <label class="block text-gray-700 font-semibold mb-2 md:mb-0 pr-4"
                                        for="googleplus">
                                        GooglePlus:
                                    </label>
                                </div>
                                <div class="md:w-2/3">
                                    <input
                                        class="border border-gray-100 transition-all delay-150 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow 
                        focus:outline-none focus:ring w-full"
                                        placeholder="test" id="googleplus" type="url" value="">
                                </div>
                            </div>
                        </div>
                        <div class="mt-10 space-x-12 hidden lg:flex justify-center items-center">
                            <button
                                class="font-FuturaBold text-primaryRed whitespace-nowrap border border-primaryRed hover:bg-primaryRed hover:text-white active:bg-blue-600 font-medium px-8 py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150"
                                type="button">
                                Back
                            </button>
                            <button
                                class="font-FuturaBold text-primaryRed whitespace-nowrap border border-primaryRed hover:bg-primaryRed hover:text-white active:bg-blue-600 font-medium px-8 py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150"
                                type="button">
                                Confirm & pay
                            </button>
                        </div>
                    </div>

                    <div>
                        <div class="lg:w-3/4 mx-auto border rounded-md">
                            <div class="bg-primaryRed text-white text-center font-semibold p-2 text-lg rounded-t-md">
                                Cart Summary</div>

                            <div class="relative overflow-x-auto">
                                <table class="w-full text-sm text-left text-gray-500">
                                    <tbody>
                                        <tr class="bg-white border-b">
                                            <!-- <th scope="row" class="px-6 py-3 font-medium text-gray-900 whitespace-nowrap">
                        Cart Item(S)
                    </th> -->
                                            <td class="px-6 py-3">
                                                Cart Item(S)
                                            </td>
                                            <td class="px-6 py-3">
                                                1
                                            </td>
                                        </tr>
                                        <tr class="bg-white border-b">
                                            <td class="px-6 py-3">
                                                Total Item Cost
                                            </td>
                                            <td class="px-6 py-3">
                                                $13.98
                                            </td>
                                        </tr>
                                        <tr class="bg-white border-b">
                                            <td class="px-6 py-3">
                                                Shipping Fees
                                            </td>
                                            <td class="px-6 py-3 text-green-500">
                                                Free
                                            </td>
                                        </tr>
                                    <tfoot>
                                        <tr class="bg-white border-b">
                                            <td class="px-6 py-3 font-medium text-gray-900 whitespace-nowrap">
                                                SubTotal
                                            </td>
                                            <td class="px-6 py-3 font-medium text-gray-900 whitespace-nowrap">
                                                $13.98
                                            </td>
                                        </tr>
                                    </tfoot>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="flex items-center justify-center space-x-3 my-8">
                            <label for="paypal"
                                class="ml-2 block font-medium text-primaryRed text-lg whitespace-nowrap">Payment
                                Method:</label>
                            <div class="flex items-center">
                                <input id="paypal" name="registration-package" type="radio"
                                    class="h-4 w-4 border-gray-300 accent-primaryRed">
                                <img class="w-28 ml-1" src="./assets/image/payment-methods/paypal.png"
                                    alt="">
                            </div>
                        </div>

                        <div class="lg:w-3/4 mx-auto border rounded-md">
                            <div class="bg-primaryRed text-white text-center font-semibold p-2 text-lg rounded-t-md">
                                More Payment Methods</div>
                            <div>
                                <!--Debit Card-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="card" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="card" class="ml-2 block font-medium text-gray-900">Credit or
                                            Debit Card</label>
                                    </div>
                                    <div class="font-medium">All Major Cards Accepted</div>
                                </div>
                                <!--Scrill-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="scrill" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="scrill"
                                            class="ml-2 block font-medium text-gray-900">Scrill</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/Skrill.png"
                                        alt="">
                                </div>
                                <!--Bank Deposit-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="bank-deposit" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="bank-deposit" class="ml-2 block font-medium text-gray-900">Bank
                                            Deposit</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/bank.png" alt="">
                                </div>
                                <!--Webmoney-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="webmoney" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="webmoney"
                                            class="ml-2 block font-medium text-gray-900">Webmoney</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/webmoney.png"
                                        alt="">
                                </div>
                                <!--Ideal-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="ideal" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="ideal"
                                            class="ml-2 block font-medium text-gray-900">IDEAL</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/ideal.png"
                                        alt="">
                                </div>
                                <!--SOFORT-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="sofort" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="sofort"
                                            class="ml-2 block font-medium text-gray-900">Sofort</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/Sofort.png"
                                        alt="">
                                </div>
                                <!--Giropay-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="giropay" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="giropay"
                                            class="ml-2 block font-medium text-gray-900">Giropay</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/giropay.png"
                                        alt="">
                                </div>
                                <!--Bancontact-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="bancontact" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="bancontact"
                                            class="ml-2 block font-medium text-gray-900">Bancontact</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/Bancontact.png"
                                        alt="">
                                </div>
                                <!--DotPay-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="dotpay" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="dotpay"
                                            class="ml-2 block font-medium text-gray-900">DotPay</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/dotpay.png"
                                        alt="">
                                </div>
                                <!--wechat pay-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="wechatpay" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="wechatpay"
                                            class="ml-2 block font-medium text-gray-900">WeChatPay</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/wechatpay.png"
                                        alt="">
                                </div>
                                <!--Alipay-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="alipay" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="alipay"
                                            class="ml-2 block font-medium text-gray-900">AliPay</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/Alipay.png"
                                        alt="">
                                </div>
                                <!--Union Pay-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="unoinpay" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="unoinpay"
                                            class="ml-2 block font-medium text-gray-900">UnionPay</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/unoinpay.png"
                                        alt="">
                                </div>
                                <!--Paytm-->
                                <div class="flex justify-between items-center p-3 border-b-2 h-20">
                                    <div class="flex items-center">
                                        <input id="paytm" name="registration-package" type="radio"
                                            class="h-4 w-4 border-gray-300 accent-primaryRed">
                                        <label for="paytm"
                                            class="ml-2 block font-medium text-gray-900">Paytm</label>
                                    </div>
                                    <img class="w-20" src="./assets/image/payment-methods/Paytm.png"
                                        alt="">
                                </div>

                            </div>

                        </div>

                    </div>


                    <div class="mt-4 space-x-5 md:space-x-10 lg:hidden flex justify-center items-center">
                        <button
                            class="font-FuturaBold text-primaryRed whitespace-nowrap border border-primaryRed hover:bg-primaryRed hover:text-white active:bg-red-600 font-medium px-8 py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150"
                            type="button">
                            Back
                        </button>
                        <button
                            class="font-FuturaBold text-primaryRed whitespace-nowrap border border-primaryRed hover:bg-primaryRed hover:text-white active:bg-red-600 font-medium px-8 py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150"
                            type="button">
                            Confirm & pay
                        </button>
                    </div>
                </div>


            </form>
        </section>
</x-web-layout>
