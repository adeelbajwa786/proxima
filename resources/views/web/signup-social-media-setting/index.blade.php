@extends('front.layouts.app')
@section('title', 'Canadian Exports | Register your account')
@section('meta_description', 'Canadian Exports is a Canadian export portal and a directory of Canadian exporters, showcasing lists of Canadian products and services, and promoting Canadian manufacturers and exporters. Canadian Exports is a Canadian business directory and a Canadian business database highlighting the Canadian industry')
@section('Canadian Export, Export from Canada, Canada export, Export Canada, Exporting from Canada, Canada export catalogue, Canada export directory, Directory of, Canadian exporters, Canada business directory, Directory of Canadian companies, Directory of Canadian companies, Canada trade, Canadian trade, Canada export portal, Canada trade mission')
@section('content')
<div class="h-screen bg-gray-50">
    <div class="py-10">
        <div class="container flex min-h-full flex-col justify-center py-12 sm:px-6 lg:px-8">
            <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-5xl">
              <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
                <h1 class="font-bold text-center text-primaryRed text-4xl lg:text-5xl mb-6"></h1>
                <social-media profile='1'></social-media>
              </div>
            </div>
          </div>
    </div>
</div>
@endsection