<section class="text-white px-4 py-6 md:p-12 desktop:px-80 bg-cover bg-center ce-export-bg">
    <h1 class="font-FuturaBold text-primaryRed text-3xl lg:text-4xl mb-6 text-center">Featured Exporters</h1>
    <p class="text-center text-xl lg:w-1/2 mx-auto px-8 mb-8">Duis aute irure dolor in reprehenderit in
        voluptate velit esse
        cillum dolore eu fugiat nulla pariatur.</p>
    <div class="relative">
        <div class="swiper exporters-slider mt-5 p-5">

            <div class="swiper-wrapper">

                <!--       start slide 1 -->
                <div class="swiper-slide slide">
                    <a href="javascript:void(0);" class="bg-white p-2 flex rounded shadow-md border">
                        <img src="{{ asset('assets/image/exporter1.png') }}" class="object-fit h-36 w-full rounded"
                            alt="Sponser" />
                    </a>
                </div>
                <!--       start slide 2 -->
                <div class="swiper-slide slide">
                    <a href="javascript:void(0);" class="bg-white p-2 flex rounded shadow-md border">
                        <img src="{{ asset('assets/image/exporter2.png') }}" class="object-fit h-36 w-full rounded"
                            alt="Sponser" />
                    </a>
                </div>
                <!--       start slide 3 -->
                <div class="swiper-slide slide">
                    <a href="javascript:void(0);" class="bg-white p-2 flex rounded shadow-md border">
                        <img src="{{ asset('assets/image/exporter3.png') }}" class="object-fit h-36 w-full rounded"
                            alt="Sponser" />
                    </a>
                </div>
                <!--       start slide 4 -->
                <div class="swiper-slide slide">
                    <a href="javascript:void(0);" class="bg-white p-2 flex rounded shadow-md border">
                        <img src="{{ asset('assets/image/exporter4.jpeg') }}" class="object-fit h-36 w-full rounded"
                            alt="Sponser" />
                    </a>
                </div>
                <!--       start slide 5 -->
                <div class="swiper-slide slide">
                    <a href="javascript:void(0);" class="bg-white p-2 flex rounded shadow-md border">
                        <img src="{{ asset('assets/image/exporter5.jpeg') }}" class="object-fit h-36 w-full rounded"
                            alt="Sponser" />
                    </a>
                </div>

            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div class="swiper-button-next">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-10 h-10 text-white">
                <path stroke-linecap="round" stroke-linejoin="round"
                    d="M12.75 15l3-3m0 0l-3-3m3 3h-7.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </div>
        <div class="swiper-button-prev">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-10 h-10 text-white">
                <path stroke-linecap="round" stroke-linejoin="round"
                    d="M11.25 9l-3 3m0 0l3 3m-3-3h7.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </div>
    </div>
</section>
