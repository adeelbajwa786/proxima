<section class="bg-white px-4 py-6 md:p-12 desktop:px-80">
    <h1 class="font-FuturaBold text-primaryRed text-3xl lg:text-4xl mb-6 text-center"> Trade Shows And Events
    </h1>
    <p class="text-center text-xl lg:w-1/2 mx-auto px-8 mb-8">Duis aute irure dolor in reprehenderit in
        voluptate velit esse
        cillum dolore eu fugiat nulla pariatur.</p>

    <div class="grid grid-cols-1 lg:grid-cols-2 gap-4 mt-6">
        <!--Card1-->
        <div class="max-w-sm w-full sm:max-w-full sm:flex shadow-md">
            <img src="{{ asset('assets/image/img1.png') }}"
                class="h-48 sm:h-auto sm:w-48 flex-none bg-cover border border-red-200 border-r-0 rounded-t sm:rounded-t-none sm:rounded-l text-center overflow-hidden" />
            <div
                class="border-r border-b border-l border-red-200 space-y-3 sm:border-l-0 sm:border-t sm:border-red-200 bg-white rounded-b sm:rounded-b-none sm:rounded-r p-4 flex flex-col justify-between leading-normal">
                <div class="mb-4 space-y-3">
                    <div class="text-gray-900 font-semibold text-xl mb-2">Lorem ipsum dolor sit amet,
                        consectetur.</div>
                    <div class="flex items-center divide-x divide-primaryRed text-primaryRed">
                        <p class="pr-3"><span>Start Date </span>2015-05-12 </p>
                        <p class="pl-3"><span>End Date </span>2015-05-14 </p>
                    </div>
                    <p class="text-gray-700 text-base">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s.
                    </p>
                </div>
                <div class="flex items-center justify-between">
                    <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4"
                        href="#">
                        See All Events
                    </a>
                    <a href="" class="text-primaryRed underline decoration-2 underline-offset-4 font-semibold">Go
                        To Website</a>
                </div>
            </div>
        </div>
        <!--Card2-->
        <div class="max-w-sm w-full md:max-w-full md:flex shadow-md">
            <img src="{{ asset('assets/image/img2.png') }}"
                class="h-48 sm:h-auto sm:w-48 flex-none bg-cover border border-red-200 border-r-0 rounded-t sm:rounded-t-none sm:rounded-l text-center overflow-hidden" />
            <div
                class="border-r border-b border-l border-red-200 space-y-3 md:border-l-0 md:border-t md:border-red-200 bg-white rounded-b md:rounded-b-none md:rounded-r p-4 flex flex-col justify-between leading-normal">
                <div class="mb-4 space-y-3">
                    <div class="text-gray-900 font-semibold text-xl mb-2">Lorem ipsum dolor sit amet,
                        consectetur.</div>
                    <div class="flex items-center divide-x divide-primaryRed text-primaryRed">
                        <p class="pr-3"><span>Start Date </span>2015-05-12 </p>
                        <p class="pl-3"><span>End Date </span>2015-05-14 </p>
                    </div>
                    <p class="text-gray-700 text-base">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s.
                    </p>
                </div>
                <div class="flex items-center justify-between">
                    <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4"
                        href="#">
                        See All Events
                    </a>
                    <a href="" class="text-primaryRed underline decoration-2 underline-offset-4 font-semibold">Go
                        To Website</a>
                </div>
            </div>
        </div>
        <!--Card3-->
        <div class="max-w-sm w-full md:max-w-full md:flex shadow-md">
            <img src="{{ asset('assets/image/img3.png') }}"
                class="h-48 sm:h-auto sm:w-48 flex-none bg-cover border border-red-200 border-r-0 rounded-t sm:rounded-t-none sm:rounded-l text-center overflow-hidden" />
            <div
                class="border-r border-b border-l border-red-200 space-y-3 md:border-l-0 md:border-t md:border-red-200 bg-white rounded-b md:rounded-b-none md:rounded-r p-4 flex flex-col justify-between leading-normal">
                <div class="mb-4 space-y-3">
                    <div class="text-gray-900 font-semibold text-xl mb-2">Lorem ipsum dolor sit amet,
                        consectetur.</div>
                    <div class="flex items-center divide-x divide-primaryRed text-primaryRed">
                        <p class="pr-3"><span>Start Date </span>2015-05-12 </p>
                        <p class="pl-3"><span>End Date </span>2015-05-14 </p>
                    </div>
                    <p class="text-gray-700 text-base">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s.
                    </p>
                </div>
                <div class="flex items-center justify-between">
                    <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4"
                        href="#">
                        See All Events
                    </a>
                    <a href="" class="text-primaryRed underline decoration-2 underline-offset-4 font-semibold">Go
                        To Website</a>
                </div>
            </div>
        </div>
        <!--Card4-->
        <div class="max-w-sm w-full md:max-w-full md:flex shadow-md">
            <img src="{{ asset('assets/image/img1.png') }}"
                class="h-48 sm:h-auto sm:w-48 flex-none bg-cover border border-red-200 border-r-0 rounded-t sm:rounded-t-none sm:rounded-l text-center overflow-hidden" />
            <div
                class="border-r border-b border-l border-red-200 space-y-3 md:border-l-0 md:border-t md:border-red-200 bg-white rounded-b md:rounded-b-none md:rounded-r p-4 flex flex-col justify-between leading-normal">
                <div class="mb-4 space-y-3">
                    <div class="text-gray-900 font-semibold text-xl mb-2">Lorem ipsum dolor sit amet,
                        consectetur.</div>
                    <div class="flex items-center divide-x divide-primaryRed text-primaryRed">
                        <p class="pr-3"><span>Start Date </span>2015-05-12 </p>
                        <p class="pl-3"><span>End Date </span>2015-05-14 </p>
                    </div>
                    <p class="text-gray-700 text-base">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s.
                    </p>
                </div>
                <div class="flex items-center justify-between">
                    <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4"
                        href="#">
                        See All Events
                    </a>
                    <a href="" class="text-primaryRed underline decoration-2 underline-offset-4 font-semibold">Go
                        To Website</a>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-10 flex justify-center">
        <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4"
            href="#">
            See All Events
        </a>
    </div>
</section>
