<div class="w-full bg-cover bg-center ce-hero-bg">
    <div
        class="flex items-center justify-center h-full w-full bg-gray-900 bg-opacity-60 py-10 sm:px-10 lg:p-24 2xl:p-52">
        <div class="text-center space-y-4 md:space-y-12 w-full lg:w-2/3 p-8">
            <div class="shadow shadow-slate-500 rounded mx-auto w-full">
                <form>
                    <div class="flex">
                        <label for="search" class="mb-2 text-sm font-medium text-gray-900 sr-only">Your Search</label>
                        <div class="relative w-full">
                            <input type="search" id="search"
                                class="block p-2 w-full z-20 text-gray-900 bg-gray-50 bg-opacity-80 rounded border border-gray-300 focus:outline-0 placeholder:text-primaryRed placeholder:font-semibold"
                                placeholder="Search your product">
                            <button type="submit"
                                class="absolute top-0 right-0 p-2.5 text-sm font-medium text-white bg-primaryRed rounded-r border border-primaryRed focus:outline-none focus:ring-0">
                                <svg aria-hidden="true" class="w-5 h-5" fill="none" stroke="currentColor"
                                    viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                                </svg>
                                <span class="sr-only">Search</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <button
                class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4 mt-4 px-4 py-2 shine bg-primaryRed text-white 2xl:text-2xl   rounded focus:outline-none focus:bg-primaryRed">
                Advanced search</button>
        </div>
    </div>
</div>
