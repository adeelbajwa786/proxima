<p class="font-semibold text-primaryRed text-lg lg:text-2xl pr-8 mb-6">
    The following business categories are full of profiles of prestigious and trustworthy Canadian
    businesses
    looking to enter your market
</p>
<div class="grid grid-cols-1 md:grid-cols-2 gap-4">
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
            class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Agriculture, Aquaculture & Livestock</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Art, Gifts, Crafts & Music</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span> Automotive & Aviation</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Business Services</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Chemicals & Pharmaceuticals</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Clothing, Footwear & Textile</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Construction: Public & Private</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Consulting</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span> Consumer Goods</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round"
                d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Control & Automation</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round"
                d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Defense & Security</span>
    </a>
    <a href="javascript:void(0);"
        class="bg-gray-100 border border-primaryRed border-opacity-30 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-7 h-7 text-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round"
                d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Electrical & Electronics</span>
    </a>
    <div class="bg-pink-100 border border-primaryRed border-opacity-50 rounded p-3 flex items-center space-x-2">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
            stroke="currentColor" class="w-8 h-8 text-white fill-primaryRed">
            <path stroke-linecap="round" stroke-linejoin="round"
                d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>How The Business Categories Came About</span>
    </div>
</div>
