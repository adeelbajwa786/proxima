<section class="bg-bgLight text-gray-800 px-4 py-6 md:p-12 desktop:px-80">
    <h1 class="font-FuturaBold text-primaryRed text-3xl lg:text-4xl mb-6 text-center">Canadian Export magazine
    </h1>
    <p class="text-center text-xl lg:w-1/2 mx-auto px-8 mb-8">Duis aute irure dolor in reprehenderit in
        voluptate velit esse
        cillum dolore eu fugiat nulla pariatur.</p>
    <div class="relative">
        <div class="swiper magazine-slider mt-5 p-5">
            <div class="swiper-wrapper">
                <!--       start slide 1 -->
                <div class="swiper-slide slide">
                    <div class="bg-cover h-96 2xl:h-[30rem] relative flex justify-center rounded shadow-md text-center overflow-hidden"
                        style="background-image: url('assets/image/magazine1.jpg')" title="">
                        <div
                            class="w-4/5 bg-white text-primaryRed border border-primaryRed rounded p-3 text-sm font-medium absolute bottom-8">
                            Published Date : Apr. 07, 2015
                        </div>
                    </div>
                </div>
                <!--       start slide 2 -->
                <div class="swiper-slide slide">
                    <div class="bg-cover h-96 2xl:h-[30rem] relative flex justify-center rounded shadow-md text-center overflow-hidden"
                        style="background-image: url('assets/image/magazine2.jpg')" title="">
                        <div
                            class="w-4/5 bg-white text-primaryRed border border-primaryRed rounded p-3 text-sm font-medium absolute bottom-8">
                            Published Date : Apr. 07, 2015
                        </div>
                    </div>
                </div>
                <!--       start slide 3 -->
                <div class="swiper-slide slide">
                    <div class="bg-cover h-96 2xl:h-[30rem] relative flex justify-center rounded shadow-md text-center overflow-hidden"
                        style="background-image: url('assets/image/magazine3.jpg')" title="">
                        <div
                            class="w-4/5 bg-white text-primaryRed border border-primaryRed rounded p-3 text-sm font-medium absolute bottom-8">
                            Published Date : Apr. 07, 2015
                        </div>
                    </div>
                </div>
                <!--       start slide 4 -->
                <div class="swiper-slide slide">
                    <div class="bg-cover h-96 2xl:h-[30rem] relative flex justify-center rounded shadow-md text-center overflow-hidden"
                        style="background-image: url('assets/image/magazine4.jpg')" title="">
                        <div
                            class="w-4/5 bg-white text-primaryRed border border-primaryRed rounded p-3 text-sm font-medium absolute bottom-8">
                            Published Date : Apr. 07, 2015
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-button-next">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-11 h-11 text-primaryRed">
                <path stroke-linecap="round" stroke-linejoin="round"
                    d="M12.75 15l3-3m0 0l-3-3m3 3h-7.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </div>
        <div class="swiper-button-prev">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-11 h-11 text-primaryRed">
                <path stroke-linecap="round" stroke-linejoin="round"
                    d="M11.25 9l-3 3m0 0l3 3m-3-3h7.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </div>
    </div>


    <div class="mt-8 flex justify-center">
        <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4" href="#">
            Sample of the previous issues
        </a>
    </div>
</section>
