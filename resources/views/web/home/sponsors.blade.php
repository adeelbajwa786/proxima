<section class="bg-white px-4 py-6 md:p-12 desktop:px-80">
    <h1 class="font-FuturaBold text-primaryRed text-3xl lg:text-4xl mb-6 text-center">Our sponsors</h1>
    <p class="text-center text-xl lg:w-1/2 mx-auto px-8 mb-8">Duis aute irure dolor in reprehenderit in
        voluptate velit esse
        cillum dolore eu fugiat nulla pariatur.</p>

    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 mt-6">
        <a href="javascript:void(0);">
            <img src="{{ asset('assets/image/sponser1.jpeg') }}" class="object-fit h-60 w-full rounded shadow-md border"
                alt="Sponser" />
        </a>
        <a href="javascript:void(0);">
            <img src="{{ asset('assets/image/sponser2.jpeg') }}" class="object-fit h-60 w-full rounded shadow-md border"
                alt="Sponser" />
        </a>
        <a href="javascript:void(0);">
            <img src="{{ asset('assets/image/sponser3.jpeg') }}" class="object-fit h-60 w-full rounded shadow-md border"
                alt="Sponser" />
        </a>
    </div>
    <div class="mt-8 flex justify-center">
        <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4"
            href="#">
            Become a sponsor
        </a>
    </div>
</section>
