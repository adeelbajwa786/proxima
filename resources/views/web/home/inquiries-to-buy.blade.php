<section class="bg-bgLight px-4 py-6 md:p-12 desktop:px-80">
    <h1 class="font-FuturaBold text-primaryRed text-3xl lg:text-4xl mb-6 text-center">Inquiries to buy</h1>

    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
        <div
            class="bg-white block shadow-md rounded p-6 md:p-4 lg:p-6 space-y-6 border border-primaryRed border-opacity-30 hover:border-opacity-100">
            <h5 class="font-semibold text-xl h-12">Space Communications System</h5>
            <div>
                <p class="md:text-sm lg:text-base"><span class="text-primaryRed">Category :
                    </span>Agriculture, Aquaculture & Livestock</p>
                <p><span class="text-primaryRed">Country : </span>Germany</p>
            </div>
            <div class="border-t flex items-center justify-between pt-3">
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Deadline
                        : </span>Apr. 07, 2015</p>
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Estimated
                        value : </span>$45,000</p>
            </div>
        </div>
        <!--2-->
        <div
            class="bg-white block shadow-md rounded p-6 md:p-4 lg:p-6 space-y-6 border border-primaryRed border-opacity-30 hover:border-opacity-100">
            <h5 class="font-semibold text-xl h-12">Purchasing System For Education
                Transport</h5>
            <div>
                <p class="md:text-sm lg:text-base"><span class="text-primaryRed">Category :
                    </span>Consumer Goods</p>
                <p><span class="text-primaryRed">Country : </span>United Kingdom</p>
            </div>
            <div class="border-t flex items-center justify-between pt-3">
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Deadline
                        : </span>June 03, 2018</p>
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Estimated
                        value : </span>$200,000</p>
            </div>
        </div>
        <!--3-->
        <div
            class="bg-white block shadow-md rounded p-6 md:p-4 lg:p-6 space-y-6 border border-primaryRed border-opacity-30 hover:border-opacity-100">
            <h5 class="font-semibold text-xl h-12">Quality Management
                Systems</h5>
            <div>
                <p class="md:text-sm lg:text-base"><span class="text-primaryRed">Category : </span>
                    Consulting</p>
                <p><span class="text-primaryRed">Country : </span>United Kingdom</p>
            </div>
            <div class="border-t flex items-center justify-between pt-3">
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Deadline
                        : </span> Aug. 21, 2018</p>
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Estimated
                        value : </span>$200,000</p>
            </div>
        </div>
        <!--4-->
        <div
            class="bg-white block shadow-md rounded p-6 md:p-4 lg:p-6 space-y-6 border border-primaryRed border-opacity-30 hover:border-opacity-100">
            <h5 class="font-semibold text-xl h-12">Selection And Executive
                Assessment Services</h5>
            <div>
                <p class="md:text-sm lg:text-base"><span class="text-primaryRed">Category : </span>
                    Consulting</p>
                <p><span class="text-primaryRed">Country : </span> Kingdom</p>
            </div>
            <div class="border-t flex items-center justify-between pt-3">
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Deadline
                        : </span>June 4, 2016</p>
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Estimated
                        value : </span>$45,180</p>
            </div>
        </div>
        <!--5-->
        <div
            class="bg-white block shadow-md rounded p-6 md:p-4 lg:p-6 space-y-6 border border-primaryRed border-opacity-30 hover:border-opacity-100">
            <h5 class="font-semibold text-xl h-12">Printing Work Provision</h5>
            <div>
                <p class="md:text-sm lg:text-base"><span class="text-primaryRed">Category :
                    </span>Business Services</p>
                <p><span class="text-primaryRed">Country : </span> Denmark</p>
            </div>
            <div class="border-t flex items-center justify-between pt-3">
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Deadline
                        : </span>Jan 23, 2018</p>
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Estimated
                        value : </span>$14.850</p>
            </div>
        </div>
        <!--6-->
        <div
            class="bg-white block shadow-md rounded p-6 md:p-4 lg:p-6 space-y-6 border border-primaryRed border-opacity-30 hover:border-opacity-100">
            <h5 class="font-semibold text-xl h-12">Services For Analysis</h5>
            <div>
                <p class="md:text-sm lg:text-base"><span class="text-primaryRed">Category :
                    </span>Business Services</p>
                <p><span class="text-primaryRed">Country : </span>France</p>
            </div>
            <div class="border-t flex items-center justify-between pt-3">
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Deadline
                        : </span>July. 16, 2020</p>
                <p class="text-sm lg:text-base"><span class="text-primaryRed text-xs lg:text-sm">Estimated
                        value : </span>$12,500</p>
            </div>
        </div>
    </div>
    <div class="mt-6 flex justify-center">
        <a class="font-FuturaBold text-primaryRed border border-primaryRed hover:bg-primaryRed hover:text-white lg:px-4 lg:py-1.5 rounded outline-none focus:outline-none ease-linear transition-all duration-150 px-4"
            href="#">
            Access all inquiries
        </a>
    </div>
</section>
