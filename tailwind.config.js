module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/**/*.{html,js,vue}',
    ],

    theme: {
        fontFamily: {
            Futura: ['Futura', "cursive"],
            FuturaItalic: ['FuturaItalic', "cursive"],
            FuturaMedium: ['FuturaMedium', "cursive"],
            FuturaBold: ['FuturaBold', "cursive"],
        },
        extend: {
            colors: {
                primary: '#0094d4',
                secondary: '#333333',
                aero: '#106BC7',
            },
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
