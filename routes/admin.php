<?php

use App\Http\Controllers\Api\Admin\{
    ArticleCategoryController,
    ArticleController,
    AuthController,
    BusinessCategoryController,
    DegreeController,
    ErrorController,
    HomePageSettingController,
    LanguageController,
    LoginPageSettingController,
    MediaController,
    MenuController,
    PageController,
    RegistrationPackageController,
    RegPageSettingController
};
use App\Http\Resources\Admin\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/user', function (Request $request) {
        $user = new UserResource($request->user());
        return response()->json(['data' => $user, 'status' => 'Success']);
    });
    Route::post('/update-profile', [AuthController::class, 'updateProfile']);
    Route::apiResource('languages', LanguageController::class);
    Route::apiResource('business-categories', BusinessCategoryController::class);
    Route::apiResource('menus', MenuController::class);
    Route::apiResource('degrees', DegreeController::class);
    Route::apiResource('article_categories', ArticleCategoryController::class);
    Route::apiResource('articles', ArticleController::class);
    Route::apiResource('pages', PageController::class);

    Route::apiResource('registration-packages', RegistrationPackageController::class);
    Route::group(['prefix' => 'reg-page-setting'], function () {
        Route::post('/', [RegPageSettingController::class, 'update']);
        Route::get('/', [RegPageSettingController::class, 'index']);
    });

    Route::group(['prefix' => 'home-page-setting'], function () {
        Route::post('/', [HomePageSettingController::class, 'update']);
        Route::get('/', [HomePageSettingController::class, 'index']);
    });


    Route::group(['prefix' => 'login-page-setting'], function () {
        Route::post('/', [LoginPageSettingController::class, 'update']);
        Route::get('/', [LoginPageSettingController::class, 'index']);
    });


    Route::group(['prefix' => 'errors'], function () {
        Route::post('/', [ErrorController::class, 'update']);
        Route::get('/', [ErrorController::class, 'index']);

    });
    Route::group(['prefix' => 'media'], function () {
        Route::post('/process', [MediaController::class, 'process']);
        Route::post('/revert', [MediaController::class, 'revert']);
        Route::get('/load', [MediaController::class, 'load']);
        Route::post('image_again_upload', [MediaController::class, 'uploadImage']);
    });
});
