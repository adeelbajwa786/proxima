<?php

use App\Http\Controllers\Api\Web\{
    AccountSettingController,
    AccountUpdateController,
    HelperController,
    PaymentController,
    ProfileController,
    SignupController
};
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('web.home.home');
// })->name('web.home');

Route::get('/', function () {
    return view('front.index');
})->name('front.index');

Route::get('/signup', function () {
    return view('front.signup.index');
})->name('web.signup')->middleware('auth.user');

Route::get('/signin', function () {
    return view('web.signin.index');
})->name('web.signin')->middleware('auth.user');


Route::group(['prefix' => 'user'], function () {
    Route::post('/logout', [SignupController::class, 'logout'])->name('web.user.logout');
});
Route::group(['prefix' => 'user', 'middleware' => ['auth.user']], function () {
    Route::post('/login', [SignupController::class, 'login'])->name('web.user.login');
    Route::get('/payment-detail', [PaymentController::class, 'index'])->name('user.payment.index');
    Route::get('/profile/{id}', [ProfileController::class, 'index'])->name('user.profile.index');
    Route::get('/account-settings',[AccountSettingController::class,'index'])->name('user.account-settings.index');
    Route::post('/update-account-settings',[AccountSettingController::class,'updateAccountSetting'])->name('user.account-settings.update');
    Route::get('/buissness-settings',[AccountSettingController::class,'buissnessSettingView'])->name('user.buissness-settings.index');
    Route::post('/update-buissness-settings',[AccountSettingController::class,'updateBussiessSetting'])->name('user.buissness-settings.update');

    Route::get('/social-media-settings',[AccountSettingController::class,'socialMediaSettingView'])->name('user.social-media-settings.index');
    Route::post('/update-social-media-settings',[AccountSettingController::class,'updateSocialMediaSetting'])->name('user.social-media-settings.update');
});

Route::get('/set-language/{language}', [HelperController::class, 'setLanguage'])->name('web.set-lang');
