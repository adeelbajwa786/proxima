<?php

use App\Http\Controllers\Api\Web\{
    SignupController,
    HelperController,
    SocialMediaController,
    UserProfileController,
    BussinessProfileController
};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'web'], function () {
    Route::post('/signup', [SignupController::class, 'signup']);
    Route::post('/social-media', [SocialMediaController::class, 'store']);
    Route::get('/show-social-media', [SocialMediaController::class, 'show']);
    Route::post('/user-profile', [UserProfileController::class, 'store']);
    Route::get('/show-user-profile', [UserProfileController::class, 'show']);
    Route::post('/bussiness-profile', [BussinessProfileController::class, 'store']);
    Route::get('/show-bussiness-profile', [BussinessProfileController::class, 'show']);
    Route::get('/get-registration-packages', [HelperController::class, 'getRegistrationPackages']);
    Route::get('/get-business-categories', [HelperController::class, 'getBusinessCategories']);
    Route::get('/get-reg-page-setting', [HelperController::class, 'getRegPageSetting']);
    Route::post('/check-customer-email', [HelperController::class, 'checkCustomerEmail']);
});



Route::group(['prefix' => 'web', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/user', function (Request $request) {
        $user = $request->user();
        return response()->json(['data' => $user, 'status' => 'Success']);
    });
});
