<?php

use App\Models\BusinessCategory;
use App\Models\BusinessCategoryDetail;
use App\Models\Language;
use App\Models\Media;
use App\Models\RegistrationPackage;
use App\Models\RegPageSetting;
use App\Models\RegPageSettingDetail;
use Cohensive\Embed\Facades\Embed;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

if (!function_exists('getAllLanguages')) {
    function getAllLanguages()
    {
        return Language::with('flagIcon')->get();
    }
}

if (!function_exists('getDefaultLanguage')) {
    function getDefaultLanguage($isWeb = false)
    {
        $lang = '';
        $webLanguage = Session::get('webLanguage');
        if ($isWeb && isset($webLanguage) && !empty($webLanguage)) {
            $lang = Language::where('id', $webLanguage)->first();
        } else {
            $lang = Language::whereIsDefault(1)->first();
        }

        return $lang ? $lang : Language::first();
    }
}

if (!function_exists("upload")) {
    function upload($file, $type)
    {
        if (isset($file)) {
            $fileName = preg_replace('/\s+/', '_', time() . ' ' . $file->getClientOriginalName());
            $path = $type . "/" . time() . "/" . $fileName;
            if (!file_exists($type . "/" . time())) {
                mkdir($type . "/" . time(), 0777);
            }
            if ($file->move($type . "/" . time(), $fileName)) {
                $media = Media::create([
                    'path' => $path,
                    'type' => 'customer_registration',
                    'extension' => pathinfo($path, PATHINFO_EXTENSION),
                ]);
                return $media->id;
            }
        }
        return null;
    }
}

if (!function_exists("getRegistrationPackage")) {
    function getRegistrationPackage($packageId)
    {
        return RegistrationPackage::whereId($packageId)->first();
    }
}


if (!function_exists("getVideoHtmlAttribute")) {
    function getVideoHtmlAttribute($url = null)
    {
        if (isset($url)) {
            $embed = Embed::make($url)->parseUrl();

            if (!$embed)
                return '';

            $embed->setAttribute(['width' => 200]);
            return $embed->getHtml();
        } else {
            return null;
        }
    }
}

if (!function_exists("getAllBusinessCategories")) {
    function getAllBusinessCategories()
    {
        $defaultLang = getDefaultLanguage(true);
        $businessCategories = BusinessCategory::addSelect(['category_name' => BusinessCategoryDetail::whereColumn('business_category_id', 'business_categories.id')->where('business_category_detail.language_id', $defaultLang->id)->select('name')])
        ->orderBy('category_name', 'asc')->get();
        return $businessCategories;
    }
}

if (!function_exists("getRegPageSetting")) {
    function getRegPageSetting()
    {
        $defaultLang = getDefaultLanguage(true);
        $regPageSetting = RegPageSetting::with(['regPageSettingDetail' => function ($q) use ($defaultLang) {
            $q->where('language_id', $defaultLang->id);
        }])->first();
        return $regPageSetting;
    }
}
