<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class HomePageSettingResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => date('m/d/Y H:i:s', strtotime($this->created_at)),
            'home_page_setting_detail' => HomePageSettingDetailResource::collection($this->whenLoaded('homePageSettingDetail')),
        ];
    }
}
