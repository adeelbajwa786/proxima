<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'url' => $this->url,
            'created_at' => date('m/d/Y H:i:s', strtotime($this->created_at)),
            'menu_detail' => MenuDetailResource::collection($this->whenLoaded('menuDetail')),
        ];
    }
}
