<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class HomePageSettingDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'home_page_setting_id' => $this->home_page_setting_id,
            'language_id' => $this->language_id,
            'welcome_title'=>$this->welcome_title,
            'welcome_description'=>$this->welcome_description,
            'welcome_button_text'=>$this->welcome_button_text,
            'welcome_button_link'=>$this->welcome_button_link,
            'welcome_image'=>$this->welcome_image,
            'featured_title'=>$this->featured_title,
            'featured_description'=>$this->featured_description,
            'financial_title'=>$this->financial_title,
            'financial_description'=>$this->financial_description,
            'banner_1_title'=>$this->banner_1_title,
            'banner_1_description'=>$this->banner_1_description,
            'banner_1_button_text'=>$this->banner_1_button_text,
            'banner_1_button_link'=>$this->banner_1_button_link,
            'banner_1_image'=>$this->banner_1_image,
            'banner_1_image_2'=>$this->banner_1_image_2,
            'work_while_study_title'=>$this->work_while_study_title,
            'work_while_study_description'=>$this->work_while_study_description,
            'banner_2_title'=>$this->banner_2_title,
            'banner_2_description'=>$this->banner_2_description,
            'banner_2_button_text'=>$this->banner_2_button_text,
            'banner_2_button_link'=>$this->banner_2_button_link,
            'banner_2_image'=>$this->banner_2_image,
            'banner_2_image_2'=>$this->banner_2_image_2,
            'proxima_title'=>$this->proxima_title,
            'proxima_description'=>$this->proxima_description,
            'recent_article_title'=>$this->recent_article_title,
            'recent_article_description'=>$this->recent_article_description,
            'banner_3_title'=>$this->banner_3_title,
            'banner_3_description'=>$this->banner_3_description,
            'banner_3_button_text'=>$this->banner_3_button_text,
            'banner_3_button_link'=>$this->banner_3_button_link,
            'banner_3_image'=>$this->banner_3_image,
            'banner_3_image_2'=>$this->banner_3_image_2,
            'home_page_setting' => new HomePageSettingResource($this->whenLoaded('homePageSetting')),
            'language' => new LanguageResource($this->whenLoaded('language')),
        ];
    }
}
