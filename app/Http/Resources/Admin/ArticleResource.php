<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'article_category_id'=>$this->article_category_id,
            'article_image' => new MediaResource($this->ArticleImage),
            'created_at' => date('m/d/Y H:i:s', strtotime($this->created_at)),
            'article_detail' => ArticleDetailResource::collection($this->whenLoaded('ArticleDetail')),
        ];
    }
}
