<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'menu_id' => $this->menu_id,
            'language_id' => $this->language_id,
            'name' => $this->name,
            'menu' => new MenuResource($this->whenLoaded('businessCategory')),
            'language' => new LanguageResource($this->whenLoaded('language')),
        ];
    }
}
