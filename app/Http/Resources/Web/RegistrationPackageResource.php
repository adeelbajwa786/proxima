<?php

namespace App\Http\Resources\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class RegistrationPackageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'free_subscription_days' => $this->free_subscription_days,
            'is_default' => $this->is_default,
            'registration_package_detail' => RegistrationPackageDetailResource::collection($this->whenLoaded('registrationPackageDetail')),
        ];
    }
}
