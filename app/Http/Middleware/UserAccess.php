<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserAccess
{

    public function handle($request, Closure $next)
    {
        $CurrentRouteName = \Request::route()->getName();
        if (Auth::guard('customers')->check()) {
            $customer = Auth::guard('customers')->user();
            if (!$customer->is_package_amount_paid && $CurrentRouteName != 'user.payment.index') {
                return redirect()->route('user.payment.index');
            } else if ($CurrentRouteName == 'web.signup' || $CurrentRouteName == 'web.signin') {
                return redirect()->route('user.account-settings.index', $customer->id);
            }
            return $next($request);
        } else {
            if ($CurrentRouteName == 'web.signup' && $CurrentRouteName == 'web.signin') {
                return redirect()->route('web.signin');
            }
        }
        return $next($request);
    }
}
