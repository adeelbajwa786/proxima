<?php

namespace App\Http\Controllers\Api\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\RegPageSettingResource;
use App\Models\BusinessCategory;
use App\Models\BusinessCategoryDetail;
use App\Models\RegistrationPackage;
use App\Models\RegPageSetting;
use App\Traits\StatusResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HelperController extends Controller
{
    use StatusResponser;


    public function getRegistrationPackages()
    {
        $defaultLang = getDefaultLanguage(1);
        $registrationPackages = RegistrationPackage::select('id', 'is_default')->with(['registrationPackageDetail' => function ($q) use ($defaultLang) {
            $q->select('id', 'registration_package_id', 'name')->where('language_id', $defaultLang->id);
        }])->get();
        return $this->successResponse($registrationPackages, 'Data Get Successfully!');
    }

    public function getBusinessCategories()
    {
        $defaultLang = getDefaultLanguage(1);
        $registrationPackages = BusinessCategory::select('id')->addSelect(['category_name' => BusinessCategoryDetail::whereColumn('business_category_id', 'business_categories.id')->where('business_category_detail.language_id', $defaultLang->id)->select('name')])
        ->orderBy('category_name', 'asc')->get();
        return $this->successResponse($registrationPackages, 'Data Get Successfully!');
    }

    public function checkCustomerEmail(Request $request)
    {
        $validationRule = [
            'email' => ['required', 'email', 'unique:App\Models\Customer,email'],
        ];

        $this->validate(
            $request,
            $validationRule
        );

        return $this->successResponse([], 'Email is valid!');
    }

    public function setLanguage($language)
    {
        Session::put('webLanguage', $language);
        return redirect()->back();
    }

    public function getRegPageSetting()
    {
        $defaultLang = getDefaultLanguage(true);
        $regPageSetting = RegPageSetting::with(['regPageSettingDetail' => function ($q) use ($defaultLang) {
            $q->where('language_id', $defaultLang->id);
        }])->first();
        return $this->successResponse(new RegPageSettingResource($regPageSetting), 'Data Get Successfully!');
    }
}
