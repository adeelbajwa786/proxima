<?php

namespace App\Http\Controllers\Api\Web;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerBusinessCategory;
use App\Models\CustomerMedia;
use App\Models\CustomerProfile;
use App\Models\CustomerSocialMedia;
use App\Traits\StatusResponser;
use App\Traits\FileUploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SignupController extends Controller
{
    use StatusResponser;
    use FileUploadTrait;


    public function signup(Request $request)
    {
        $request['business_categories_id'] = json_decode($request->business_categories_id);
        $validationRule = [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:App\Models\Customer,email'],
            'password' => ['required', 'confirmed', 'min:6'],
            'is_agree' => ['required', 'accepted'],
            'registration_package_id' => ['required', 'exists:App\Models\RegistrationPackage,id'],
            'business_categories_id' => ['required', 'array', 'max:3'],
            'business_categories_id.*' => ['required', 'exists:App\Models\BusinessCategory,id'],
            'customer_profile_address' => ['required', 'string'],
            'customer_profile_company_email' => ['required', 'email'],
            'customer_profile_company_name' => ['required', 'string'],
            'customer_profile_description' => ['required', 'string'],
            'customer_profile_keywords' => ['required', 'string'],
            'customer_profile_phone' => ['required', 'string'],
            'customer_profile_short_description' => ['required', 'string'],
            'customer_profile_website' => ['required', 'url'],
            'customer_media_video' => ['nullable', 'url'],
            'customer_media_image_1' => ['nullable', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048'],
            'customer_media_image_2' => ['nullable', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048'],
            'customer_media_image_3' => ['nullable', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048'],
            'customer_media_image_4' => ['nullable', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048'],
            'customer_media_logo' => ['nullable', 'image', 'mimes:jpeg,jpg,png,gif', 'max:2048'],
            'customer_social_media_facebook' => ['nullable', 'url'],
            'customer_social_media_linked_in' => ['nullable', 'url'],
            'customer_social_media_twitter' => ['nullable', 'url'],
            'customer_social_media_youtube' => ['nullable', 'url'],
        ];
        // $errorMessages = [
        //     'is_agree.required' => 'You must agree with terms and conditions.',
        //     'is_agree.accepted' => 'You must agree with terms and conditions.',
        //     'registration_package_id.required' => 'The registration package is required.',
        //     'registration_package_id.exists' => 'The selected registration package is invalid.',
        //     'business_categories_id.required' => 'The business categories is required.',
        //     'business_categories_id.array' => 'The business categories must be an array.',
        //     'business_categories_id.max' => 'The business categories must not have more than 3 items.',
        //     'business_categories_id.*.required' => 'The business categories is required.',
        //     'business_categories_id.*.exists' => 'The selected business categories is invalid.',
        //     'customer_profile_address.required' => 'The address field is required.',
        //     'customer_profile_company_email.required' => 'The company email field is required.',
        //     'customer_profile_company_email.email' => 'The company email must be a valid email address.',
        //     'customer_profile_company_name.required' => 'The company name field is required.',
        //     'customer_profile_description.required' => 'The Description field is required.',
        //     'customer_profile_keywords.required' => 'The keyword field is required.',
        //     'customer_profile_phone.required' => 'The phone field is required.',
        //     'customer_profile_short_description.required' => 'The short description field is required.',
        //     'customer_profile_website.required' => 'The website field is required.',
        //     'customer_profile_website.url' => 'The website must be a valid URL.',
        //     'customer_media_video.url' => 'The video must be a valid URL.',
        //     'customer_media_image_1.image' => 'The image 1 must be an image.',
        //     'customer_media_image_1.mimes' => 'The image 1 must be a file of type: jpeg,jpg,png,gif.',
        //     'customer_media_image_1.max' => 'The image 1 must not be greater than 2048 kilobytes.',
        //     'customer_media_image_2.image' => 'The image 2 must be an image.',
        //     'customer_media_image_2.mimes' => 'The image 2 must be a file of type: jpeg,jpg,png,gif.',
        //     'customer_media_image_2.max' => 'The image 2 must not be greater than 2048 kilobytes.',
        //     'customer_media_image_3.image' => 'The image 3 must be an image.',
        //     'customer_media_image_3.mimes' => 'The image 3 must be a file of type: jpeg,jpg,png,gif.',
        //     'customer_media_image_3.max' => 'The image 3 must not be greater than 2048 kilobytes.',
        //     'customer_media_image_4.image' => 'The image 4 must be an image.',
        //     'customer_media_image_4.mimes' => 'The image 4 must be a file of type: jpeg,jpg,png,gif.',
        //     'customer_media_image_4.max' => 'The image 4 must not be greater than 2048 kilobytes.',
        //     'customer_media_logo.image' => 'The logo must be an image.',
        //     'customer_media_logo.mimes' => 'The logo must be a file of type: jpeg,jpg,png,gif.',
        //     'customer_media_logo.max' => 'The logo must not be greater than 2048 kilobytes.',
        //     'customer_social_media_facebook.url' => 'Then facebook must be a valid URL.',
        //     'customer_social_media_linked_in.url' => 'Then linkedIn must be a valid URL.',
        //     'customer_social_media_twitter.url' => 'Then twitter must be a valid URL.',
        //     'customer_social_media_youtube.url' => 'Then youtube must be a valid URL.',
        // ];
        $defaulLang = getDefaultLanguage(1);
        if($defaulLang){
            App::setLocale($defaulLang->abbreviation);
            $regPageSetting = getRegPageSetting();
            $regPageSettingDetail = $regPageSetting->regPageSettingDetail;
            $niceNames = [
                'name' => isset($regPageSettingDetail[0]->step_2_name_label) ? $regPageSettingDetail[0]->step_2_name_label : '',
                'email' => isset($regPageSettingDetail[0]->step_2_email_label) ? $regPageSettingDetail[0]->step_2_email_label : '',
                'password' => isset($regPageSettingDetail[0]->step_2_password_label) ? $regPageSettingDetail[0]->step_2_password_label : '',
                'password_confirmation' => isset($regPageSettingDetail[0]->step_2_confirm_password_label) ? $regPageSettingDetail[0]->step_2_confirm_password_label : '',
                // 'is_agree' => isset($regPageSettingDetail[0]->step_2_confirm_password_label) ? $regPageSettingDetail[0]->step_2_confirm_password_label : '',
                // 'registration_package_id' => isset($regPageSettingDetail[0]->step_2_confirm_password_label) ? $regPageSettingDetail[0]->step_2_confirm_password_label : '',
                'business_categories_id' => isset($regPageSettingDetail[0]->step_3_business_categories_label) ? $regPageSettingDetail[0]->step_3_business_categories_label : '',
                'customer_profile_address' => isset($regPageSettingDetail[0]->step_4_address_label) ? $regPageSettingDetail[0]->step_4_address_label : '',
                'customer_profile_company_email' => isset($regPageSettingDetail[0]->step_4_email_label) ? $regPageSettingDetail[0]->step_4_email_label : '',
                'customer_profile_company_name' => isset($regPageSettingDetail[0]->step_4_name_label) ? $regPageSettingDetail[0]->step_4_name_label : '',
                'customer_profile_description' => isset($regPageSettingDetail[0]->step_4_description_label) ? $regPageSettingDetail[0]->step_4_description_label : '',
                'customer_profile_keywords' => isset($regPageSettingDetail[0]->step_4_keywords_label) ? $regPageSettingDetail[0]->step_4_keywords_label : '',
                'customer_profile_phone' => isset($regPageSettingDetail[0]->step_4_phone_label) ? $regPageSettingDetail[0]->step_4_phone_label : '',
                'customer_profile_short_description' => isset($regPageSettingDetail[0]->step_4_short_description_label) ? $regPageSettingDetail[0]->step_4_short_description_label : '',
                'customer_profile_website' => isset($regPageSettingDetail[0]->step_4_website_label) ? $regPageSettingDetail[0]->step_4_website_label : '',
                'customer_media_video' => isset($regPageSettingDetail[0]->step_5_video_label) ? $regPageSettingDetail[0]->step_5_video_label : '',
                'customer_media_image_1' => isset($regPageSettingDetail[0]->step_5_image_1_label) ? $regPageSettingDetail[0]->step_5_image_1_label : '',
                'customer_media_image_2' => isset($regPageSettingDetail[0]->step_5_image_2_label) ? $regPageSettingDetail[0]->step_5_image_2_label : '',
                'customer_media_image_3' => isset($regPageSettingDetail[0]->step_5_image_3_label) ? $regPageSettingDetail[0]->step_5_image_3_label : '',
                'customer_media_image_4' => isset($regPageSettingDetail[0]->step_5_image_4_label) ? $regPageSettingDetail[0]->step_5_image_4_label : '',
                'customer_media_logo' => isset($regPageSettingDetail[0]->step_5_logo_label) ? $regPageSettingDetail[0]->step_5_logo_label : '',
                'customer_social_media_facebook' => isset($regPageSettingDetail[0]->step_6_facebook_label) ? $regPageSettingDetail[0]->step_6_facebook_label : '',
                'customer_social_media_linked_in' => isset($regPageSettingDetail[0]->step_6_linkedin_label) ? $regPageSettingDetail[0]->step_6_linkedin_label : '',
                'customer_social_media_twitter' => isset($regPageSettingDetail[0]->step_6_twitter_label) ? $regPageSettingDetail[0]->step_6_twitter_label : '',
                'customer_social_media_youtube' => isset($regPageSettingDetail[0]->step_6_youtube_label) ? $regPageSettingDetail[0]->step_6_youtube_label : '',
            ]; 
        }
        $this->validate(
            $request,
            $validationRule,
            [],
            $niceNames
        );
        $package = getRegistrationPackage($request->registration_package_id);
        $customer = Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'registration_package_id' => $request->registration_package_id,
            'package_price' => isset($package) ? $package->price : null,
            'free_subscription_days' => isset($package) ? $package->free_subscription_days : null,
            'package_subscribed_date' => date('Y-m-d'),
            'package_expiry_date' => date('Y-m-d'),
            'is_package_amount_paid' => isset($package) && $package->price <= 0 ? 1 : 0,
        ]);

        CustomerProfile::create([
            'company_name' => $request->customer_profile_company_name,
            'company_email' => $request->customer_profile_company_email,
            'short_description' => $request->customer_profile_short_description,
            'description' => $request->customer_profile_description,
            'phone' => $request->customer_profile_phone,
            'website' => $request->customer_profile_website,
            'keywords' => json_decode($request->customer_profile_keywords, 1),
            'address' => $request->customer_profile_address,
            'customer_id' => $customer->id
        ]);

        foreach ($request->business_categories_id as $business_category_id) {
            CustomerBusinessCategory::create([
                'business_category_id' => $business_category_id,
                'customer_id' => $customer->id,
            ]);
        }


        CustomerMedia::create([
            "logo" => upload($request->customer_media_logo, "media/customers"),
            "video" => getVideoHtmlAttribute($request->customer_media_video),
            "image_1" => upload($request->customer_media_image_1, "media/customers"),
            "image_2" => upload($request->customer_media_image_2, "media/customers"),
            "image_3" => upload($request->customer_media_image_3, "media/customers"),
            "image_4" => upload($request->customer_media_image_4, "media/customers"),
            "customer_id" => $customer->id
        ]);

        CustomerSocialMedia::create([
            "facebook" => $request->customer_social_media_facebook,
            "twitter" => $request->customer_social_media_twitter,
            "youtube" => $request->customer_social_media_youtube,
            "linked_in" => $request->customer_social_media_linked_in,
            "customer_id" => $customer->id
        ]);

        Auth::guard('customers')->login($customer);

        $data = [];
        if ($package->price > 0) {
            $data['redirect_url'] = route('user.payment.index');
        } else {
            $data['redirect_url'] = route('user.profile.index', $customer->id);
        }
        return $this->successResponse($data, 'Customer profile has been created successfully!');
    }

    public function logout()
    {
        if (Auth::guard('customers')->check()) {
            Auth::guard('customers')->logout();
        }
        return redirect()->route('web.signin');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::guard('customers')->attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->route('user.account-settings.index', Auth::guard('customers')->user()->id);
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }
}
