<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\MenuResource;
use Illuminate\Support\Str;
use App\Models\Menu;
use App\Models\MenuDetail;
use App\Models\Language;
use App\Rules\CheckCategorySlug;
use App\Traits\StatusResponser;
use App\Traits\FileUploadTrait;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    use StatusResponser;
    use FileUploadTrait;


    public function index()
    {
        $menu = Menu::query();

        $menu = $this->whereClause($menu);
        $menu = $this->loadRelations($menu);
        $menu = $this->sortingAndLimit($menu);

        return $this->apiSuccessResponse(MenuResource::collection($menu), 'Data Get Successfully!');
    }


    public function show(Menu $menu)
    {
        if (isset($_GET['withMenuDetail']) && $_GET['withMenuDetail'] == '1') {
            $menu = $menu->loadMissing('menuDetail');
        }

        return $this->apiSuccessResponse(new MenuResource($menu), 'Data Get Successfully!');
    }


    public function store(Request $request)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );
        $menu = Menu::create(['url'=> $request->url]);
        foreach ($languages as $language) {
            MenuDetail::create([
                'menu_id' => $menu->id,
                'language_id' => $language->id,
                'name' => $request['name']['name_' . $language->id]
            ]);
        }

        if ($menu) {
            return $this->apiSuccessResponse(new MenuResource($menu), 'Business category has been added successfully.');
        }
        return $this->errorResponse();
    }


    public function update(Request $request, Menu $menu)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, $menu->id)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );


        $menu->update(['url'=> $request->url]);
        foreach ($languages as $language) {
            $menuDetail = MenuDetail::whereLanguageId($language->id)->whereMenuId($menu->id)->exists();
            if ($menuDetail) {
                MenuDetail::whereLanguageId($language->id)->whereMenuId($menu->id)->update([
                    'name' => $request['name']['name_' . $language->id]
                ]);
            } else {
                MenuDetail::create([
                    'menu_id' => $menu->id,
                    'language_id' => $language->id,
                    'name' => $request['name']['name_' . $language->id]
                ]);
            }
        }

        if ($menu) {
            return $this->apiSuccessResponse(new MenuResource($menu), 'Business category has been updated successfully.');
        }
        return $this->errorResponse();
    }


    public function destroy(Menu $menu)
    {
        if ($menu->menuDetail()->delete() && $menu->delete()) {
            return $this->apiSuccessResponse(new MenuResource($menu), 'Business category has been deleted successfully.');
        }
        return $this->errorResponse();
    }

    protected function loadRelations($menu)
    {
        $defaultLang = getDefaultLanguage();
        $menu = $menu->with('menuDetail');
        if (isset($_GET['withMenuDetail']) && $_GET['withMenuDetail'] == '1') {
            $menu = $menu->with('menuDetail');
        }
        return $menu;
    }

    protected function sortingAndLimit($menu)
    {
        $sortType = ['ASC', 'asc', 'DESC', 'desc'];
        $sortBy = ['id', 'name', 'abbreviation', 'Business category_name'];
        if (isset($_GET['sortBy']) && $_GET['sortBy'] != '' && isset($_GET['sortType']) && $_GET['sortType'] != '' && in_array($_GET['sortBy'], $sortBy) && in_array($_GET['sortType'], $sortType)) {
            $menu = $menu->OrderBy($_GET['sortBy'], $_GET['sortType']);
        }

        if (isset($_GET['limit']) && $_GET['limit'] != '') {
            $limit = $_GET['limit'];
        } else {
            $limit = 10;
        }

        return $menu->paginate($limit);
    }

    protected function whereClause($menu)
    {
        if (isset($_GET['searchParam']) && $_GET['searchParam'] != '') {
            $menu = $menu->whereHas('menuDetail', function ($q) {
                $q->where('name', 'LIKE', '%' . $_GET['searchParam'] . '%');
            });
        }
        return $menu;
    }
}
