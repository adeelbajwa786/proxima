<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\TemporaryMedia;
use App\Traits\FileUploadTrait;
use App\Traits\StatusResponser;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    use StatusResponser;
    use FileUploadTrait;


    public function process(Request $request)
    {
        if (isset($request->is_temp_media) && $request->is_temp_media) {
            $destinationFolder = 'temp/' . uniqid();
        }

        $media = $this->saveFiles($request->media, $destinationFolder);
        foreach ($media as $file) {
            TemporaryMedia::create([
                'path' => $file,
                'type' => $request->type,
                'extension' => pathinfo($file, PATHINFO_EXTENSION),
            ]);
        }

        return $media;
    }

    public function revert(Request $request)
    {
        $media = $request->media;
        $media = json_decode($media, 1);
        $result = $this->removeFile($media);
        if ($result) {
            foreach ($media as $file) {
                TemporaryMedia::wherePath($file)->delete();
            }
            return $result;
        }
        return false;
    }

    public function uploadImage(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        $image = $request->file('file');
        $name = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $name);
        return '/images/'.$name;
    }
}
