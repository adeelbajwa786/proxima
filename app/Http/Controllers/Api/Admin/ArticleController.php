<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\ArticleResource;
use Illuminate\Support\Str;
use App\Models\Article;
use App\Models\ArticleDetail;
use App\Models\Language;
use App\Rules\CheckCategorySlug;
use App\Traits\StatusResponser;
use App\Traits\FileUploadTrait;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    use StatusResponser;
    use FileUploadTrait;


    public function index()
    {
        $articles = Article::query();

        $articles = $this->whereClause($articles);
        $articles = $this->loadRelations($articles);
        $articles = $this->sortingAndLimit($articles);

        return $this->apiSuccessResponse(ArticleResource::collection($articles), 'Data Get Successfully!');
    }


    public function show(Article $Article)
    {
        if (isset($_GET['withArticleDetail']) && $_GET['withArticleDetail'] == '1') {
            $Article = $Article->loadMissing('ArticleDetail');
        }

        return $this->apiSuccessResponse(new ArticleResource($Article), 'Data Get Successfully!');
    }


    public function store(Request $request)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['article_category_id' => ['required', 'string','exists:article_categories,id']]);
            $errorMessages = array_merge($errorMessages, ['article_category_id.required' => 'Article Category is required.']);
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
            $validationRule = array_merge($validationRule, ['short_description.short_description_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['short_description.short_description_' . $language->id . '.required' => 'Short Description in ' . $language->name . ' is required.']);
            $validationRule = array_merge($validationRule, ['description.description_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['description.description_' . $language->id . '.required' => 'Description in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );
        $media = json_decode($request->article_image, 1);
        $files = $this->moveFile($media, 'media/images', 'article_image');

        $Article = Article::create([
            'article_image'=>isset($files, $files[0]) ? $files[0]->id : null,
            'article_category_id'=>$request->article_category_id
        ]);
        

        if ($Article) {
            foreach ($languages as $language) {
                ArticleDetail::create([
                    'article_id' => $Article->id,
                    'language_id' => $language->id,
                    'name' => $request['name']['name_' . $language->id],
                    'description' => $request['description']['description_' . $language->id],
                    'short_description' => $request['short_description']['short_description_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                ]);
            }
            return $this->apiSuccessResponse(new ArticleResource($Article), 'Article has been added successfully.');
        }
        return $this->errorResponse();
    }


    public function update(Request $request, Article $Article)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['article_category_id' => ['required', 'string','exists:article_categories,id']]);
            $errorMessages = array_merge($errorMessages, ['article_category_id.required' => 'Article Category is required.']);
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
            $validationRule = array_merge($validationRule, ['short_description.short_description_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['short_description.short_description_' . $language->id . '.required' => 'Short Description in ' . $language->name . ' is required.']);
            $validationRule = array_merge($validationRule, ['description.description_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['description.description_' . $language->id . '.required' => 'Description in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );

        if (isset($request->article_image) && !is_array($request->article_image)) {
            $media = json_decode($request->article_image, 1);
            $files = $this->moveFile($media, 'media/images', 'article_image');
        }
        $Article->update([
            'article_image' => !isset($request->article_image) ? null : (isset($files, $files[0]) ? $files[0]->id : $Article->article_image),
            'article_category_id'=> $request->article_category_id
        ]);
        foreach ($languages as $language) {
            $ArticleDetail = ArticleDetail::whereLanguageId($language->id)->whereArticleId($Article->id)->exists();
            if ($ArticleDetail) {
                ArticleDetail::whereLanguageId($language->id)->whereArticleId($Article->id)->update([
                    'name' => $request['name']['name_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                    'description' => $request['description']['description_' . $language->id],
                    'short_description' => $request['short_description']['short_description_' . $language->id],
                ]);
            } else {
                ArticleDetail::create([
                    'article_id' => $Article->id,
                    'language_id' => $language->id,
                    'name' => $request['name']['name_' . $language->id],
                    'description' => $request['description']['description_' . $language->id],
                    'short_description' => $request['short_description']['short_description_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                ]);
            }
        }

        if ($Article) {
            return $this->apiSuccessResponse(new ArticleResource($Article), 'Article has been updated successfully.');
        }
        return $this->errorResponse();
    }


    public function destroy(Article $Article)
    {
        if ($Article->ArticleDetail()->delete() && $Article->delete()) {
            return $this->apiSuccessResponse(new ArticleResource($Article), 'Article has been deleted successfully.');
        }
        return $this->errorResponse();
    }

    protected function loadRelations($articles)
    {
        $defaultLang = getDefaultLanguage();
        $articles = $articles->with(['ArticleDetail' => function ($q) use ($defaultLang) {
            $q->where('language_id', $defaultLang->id);
        }]);
        if (isset($_GET['withArticleDetail']) && $_GET['withArticleDetail'] == '1') {
            $articles = $articles->with('ArticleDetail');
        }
        return $articles;
    }

    protected function sortingAndLimit($articles)
    {
        $sortType = ['ASC', 'asc', 'DESC', 'desc'];
        $sortBy = ['id', 'name', 'abbreviation', 'article_name'];
        if (isset($_GET['sortBy']) && $_GET['sortBy'] != '' && isset($_GET['sortType']) && $_GET['sortType'] != '' && in_array($_GET['sortBy'], $sortBy) && in_array($_GET['sortType'], $sortType)) {
            $articles = $articles->OrderBy($_GET['sortBy'], $_GET['sortType']);
        }

        if (isset($_GET['limit']) && $_GET['limit'] != '') {
            $limit = $_GET['limit'];
        } else {
            $limit = 10;
        }

        return $articles->paginate($limit);
    }

    protected function whereClause($articles)
    {
        if (isset($_GET['searchParam']) && $_GET['searchParam'] != '') {
            $articles = $articles->whereHas('ArticleDetail', function ($q) {
                $q->where('name', 'LIKE', '%' . $_GET['searchParam'] . '%');
            });
        }
        return $articles;
    }
}
