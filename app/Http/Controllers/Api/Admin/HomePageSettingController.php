<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\HomePageSettingResource;
use App\Models\HomePageSetting;
use App\Models\HomePageSettingDetail;
use App\Traits\StatusResponser;
use Illuminate\Http\Request;

class HomePageSettingController extends Controller
{
    use StatusResponser;

    public function index()
    {
        $homePageSetting = HomePageSetting::with('homePageSettingDetail')->first();

        if(!$homePageSetting){
            $homePageSetting = HomePageSetting::create([]);
            $languages = getAllLanguages();
            foreach ($languages as $language) {
                HomePageSettingDetail::create([
                    'home_page_setting_id' => $homePageSetting->id,
                    'language_id' => $language->id,
                ]);
            }
            $homePageSetting = $homePageSetting->loadMissing('homePageSettingDetail');
        }

        return $this->successResponse(new HomePageSettingResource($homePageSetting), 'Data get successfully.');
    }

    public function update(Request $request)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['welcome_title.welcome_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['welcome_title.welcome_title_' . $language->id . '.required' => 'This field is required.']);
            
            $validationRule = array_merge($validationRule, ['welcome_description.welcome_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['welcome_description.welcome_description_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['welcome_button_text.welcome_button_text_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['welcome_button_text.welcome_button_text_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['welcome_button_link.welcome_button_link_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['welcome_button_link.welcome_button_link_' . $language->id . '.required' => 'This field is required.']);
            
            // first name
            $validationRule = array_merge($validationRule, ['welcome_image.welcome_image_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['welcome_image.welcome_image_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['featured_title.featured_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['featured_title.featured_title_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['featured_description.featured_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['featured_description.featured_description_' . $language->id . '.required' => 'This field is required.']);

            // last name
            $validationRule = array_merge($validationRule, ['financial_title.financial_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['financial_title.financial_title_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['financial_description.financial_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['financial_description.financial_description_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_1_title.banner_1_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_1_title.banner_1_title_' . $language->id . '.required' => 'This field is required.']);

      
            //password
            $validationRule = array_merge($validationRule, ['banner_1_description.banner_1_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_1_description.banner_1_description_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_1_button_text.banner_1_button_text_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_1_button_text.banner_1_button_text_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_1_button_link.banner_1_button_link_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_1_button_link.banner_1_button_link_' . $language->id . '.required' => 'This field is required.']);

            //confirm password
            $validationRule = array_merge($validationRule, ['banner_1_image.banner_1_image_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_1_image.banner_1_image_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_1_image_2.banner_1_image_2_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_1_image_2.banner_1_image_2_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['work_while_study_title.work_while_study_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['work_while_study_title.work_while_study_title_' . $language->id . '.required' => 'This field is required.']);


            $validationRule = array_merge($validationRule, ['work_while_study_description.work_while_study_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['work_while_study_description.work_while_study_description_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_2_title.banner_2_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_2_title.banner_2_title_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_2_description.banner_2_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_2_description.banner_2_description_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_2_button_text.banner_2_button_text_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_2_button_text.banner_2_button_text_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_2_button_link.banner_2_button_link_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_2_button_link.banner_2_button_link_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_2_image.banner_2_image_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_2_image.banner_2_image_' . $language->id . '.required' => 'This field is required.']);


            $validationRule = array_merge($validationRule, ['banner_2_image_2.banner_2_image_2_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_2_image_2.banner_2_image_2_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['proxima_title.proxima_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['proxima_title.proxima_title_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['proxima_description.proxima_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['proxima_description.proxima_description_' . $language->id . '.required' => 'This field is required.']);



            $validationRule = array_merge($validationRule, ['recent_article_title.recent_article_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['recent_article_title.recent_article_title_' . $language->id . '.required' => 'This field is required.']);



            $validationRule = array_merge($validationRule, ['recent_article_description.recent_article_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['recent_article_description.recent_article_description_' . $language->id . '.required' => 'This field is required.']);



            $validationRule = array_merge($validationRule, ['banner_3_title.banner_3_title_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_3_title.banner_3_title_' . $language->id . '.required' => 'This field is required.']);


            $validationRule = array_merge($validationRule, ['banner_3_description.banner_3_description_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_3_description.banner_3_description_' . $language->id . '.required' => 'This field is required.']);


            $validationRule = array_merge($validationRule, ['banner_3_button_text.banner_3_button_text_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_3_button_text.banner_3_button_text_' . $language->id . '.required' => 'This field is required.']);




            $validationRule = array_merge($validationRule, ['banner_3_button_link.banner_3_button_link_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_3_button_link.banner_3_button_link_' . $language->id . '.required' => 'This field is required.']);



            $validationRule = array_merge($validationRule, ['banner_3_image.banner_3_image_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_3_image.banner_3_image_' . $language->id . '.required' => 'This field is required.']);

            $validationRule = array_merge($validationRule, ['banner_3_image_2.banner_3_image_2_' . $language->id => ['required', 'string']]);
            $errorMessages = array_merge($errorMessages, ['banner_3_image_2.banner_3_image_2_' . $language->id . '.required' => 'This field is required.']);
            
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );

        $homePageSetting = HomePageSetting::firstOrCreate([]);
        $homePageSetting->touch();
        foreach ($languages as $language) {
            $homePageSettingDetail = HomePageSettingDetail::whereLanguageId($language->id)->whereHomePageSettingId($homePageSetting->id)->exists();
            
            $fields = [
                'welcome_title' => $request['welcome_title']['welcome_title_' . $language->id],
                'welcome_image' => $request['welcome_image']['welcome_image_' . $language->id],
                'featured_title' => $request['featured_title']['featured_title_' . $language->id],
                'featured_description' => $request['featured_description']['featured_description_' . $language->id],
                'financial_title' => $request['financial_title']['financial_title_' . $language->id],
                'financial_description' => $request['financial_description']['financial_description_' . $language->id],
                'banner_1_title' => $request['banner_1_title']['banner_1_title_' . $language->id],
                'welcome_description' => $request['welcome_description']['welcome_description_' . $language->id],
                'welcome_button_text' => $request['welcome_button_text']['welcome_button_text_' . $language->id],
                'welcome_button_link' => $request['welcome_button_link']['welcome_button_link_' . $language->id],
                'banner_1_description' => $request['banner_1_description']['banner_1_description_' . $language->id],
                'banner_1_button_text' => $request['banner_1_button_text']['banner_1_button_text_' . $language->id],
                'banner_1_button_link' => $request['banner_1_button_link']['banner_1_button_link_' . $language->id],
                
                'banner_1_image' => $request['banner_1_image']['banner_1_image_' . $language->id],
                'banner_1_image_2' => $request['banner_1_image_2']['banner_1_image_2_' . $language->id],
                'work_while_study_title' => $request['work_while_study_title']['work_while_study_title_' . $language->id],
                'work_while_study_description' => $request['work_while_study_description']['work_while_study_description_' . $language->id],
                'banner_2_title' => $request['banner_2_title']['banner_2_title_' . $language->id],
                'banner_2_description' => $request['banner_2_description']['banner_2_description_' . $language->id],
                'banner_2_button_text' => $request['banner_2_button_text']['banner_2_button_text_' . $language->id],
                'banner_2_button_link' => $request['banner_2_button_link']['banner_2_button_link_' . $language->id],
                'banner_2_image' => $request['banner_2_image']['banner_2_image_' . $language->id],
                'banner_2_image_2' => $request['banner_2_image_2']['banner_2_image_2_' . $language->id],
                'proxima_title' => $request['proxima_title']['proxima_title_' . $language->id],
                'proxima_description' => $request['proxima_description']['proxima_description_' . $language->id],
                'recent_article_title' => $request['recent_article_title']['recent_article_title_' . $language->id],
                'recent_article_description' => $request['recent_article_description']['recent_article_description_' . $language->id],
                'banner_3_title' => $request['banner_3_title']['banner_3_title_' . $language->id],
                'banner_3_description' => $request['banner_3_description']['banner_3_description_' . $language->id],
                'banner_3_button_text' => $request['banner_3_button_text']['banner_3_button_text_' . $language->id],
                'banner_3_button_link' => $request['banner_3_button_link']['banner_3_button_link_' . $language->id],
                'banner_3_image' => $request['banner_3_image']['banner_3_image_' . $language->id],
                'banner_3_image_2' => $request['banner_3_image_2']['banner_3_image_2_' . $language->id]

            ];
            if ($homePageSettingDetail) {
                HomePageSettingDetail::whereLanguageId($language->id)->whereHomePageSettingId($homePageSetting->id)->update($fields);
            } else {
                $fields = array_merge($fields, ['home_page_setting_id' => $homePageSetting->id]);
                $fields = array_merge($fields, ['language_id' => $language->id]);
                HomePageSettingDetail::create($fields);
            }
        }

        if ($homePageSetting) {
            return $this->apiSuccessResponse(new HomePageSettingResource($homePageSetting), 'Setting has been updated successfully.');
        }
        return $this->errorResponse();
    }
}
