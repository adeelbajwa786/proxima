<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\DegreeResource;
use Illuminate\Support\Str;
use App\Models\Degree;
use App\Models\DegreeDetail;
use App\Models\Language;
use App\Rules\CheckCategorySlug;
use App\Traits\StatusResponser;
use App\Traits\FileUploadTrait;
use Illuminate\Http\Request;

class DegreeController extends Controller
{
    use StatusResponser;
    use FileUploadTrait;


    public function index()
    {
        $degrees = Degree::query();

        $degrees = $this->whereClause($degrees);
        $degrees = $this->loadRelations($degrees);
        $degrees = $this->sortingAndLimit($degrees);

        return $this->apiSuccessResponse(DegreeResource::collection($degrees), 'Data Get Successfully!');
    }


    public function show(Degree $Degree)
    {
        if (isset($_GET['withDegreeDetail']) && $_GET['withDegreeDetail'] == '1') {
            $Degree = $Degree->loadMissing('DegreeDetail');
        }

        return $this->apiSuccessResponse(new DegreeResource($Degree), 'Data Get Successfully!');
    }


    public function store(Request $request)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );
        $media = json_decode($request->degree_image, 1);
        $files = $this->moveFile($media, 'media/images', 'degree_image');
        $Degree = Degree::create(['degree_image'=> isset($files, $files[0]) ? $files[0]->id : null]);
        

        if ($Degree) {
            foreach ($languages as $language) {
                DegreeDetail::create([
                    'degree_id' => $Degree->id,
                    'language_id' => $language->id,
                    'name' => $request['name']['name_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                ]);
            }
            return $this->apiSuccessResponse(new DegreeResource($Degree), 'Business category has been added successfully.');
        }
        return $this->errorResponse();
    }


    public function update(Request $request, Degree $Degree)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, $Degree->id)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );

        if (isset($request->degree_image) && !is_array($request->degree_image)) {
            $media = json_decode($request->degree_image, 1);
            $files = $this->moveFile($media, 'media/images', 'degree_image');
        }

        $Degree->update([
            'degree_image' => !isset($request->degree_image) ? null : (isset($files, $files[0]) ? $files[0]->id : $Degree->degree_image),
        ]);
        foreach ($languages as $language) {
            $DegreeDetail = DegreeDetail::whereLanguageId($language->id)->whereDegreeId($Degree->id)->exists();
            if ($DegreeDetail) {
                DegreeDetail::whereLanguageId($language->id)->whereDegreeId($Degree->id)->update([
                    'name' => $request['name']['name_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                ]);
            } else {
                DegreeDetail::create([
                    'degree_id' => $Degree->id,
                    'language_id' => $language->id,
                    'name' => $request['name']['name_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                ]);
            }
        }

        if ($Degree) {
            return $this->apiSuccessResponse(new DegreeResource($Degree), 'Business category has been updated successfully.');
        }
        return $this->errorResponse();
    }


    public function destroy(Degree $Degree)
    {
        if ($Degree->DegreeDetail()->delete() && $Degree->delete()) {
            return $this->apiSuccessResponse(new DegreeResource($Degree), 'Business category has been deleted successfully.');
        }
        return $this->errorResponse();
    }

    protected function loadRelations($degrees)
    {
        $defaultLang = getDefaultLanguage();
        $degrees = $degrees->with(['DegreeDetail' => function ($q) use ($defaultLang) {
            $q->where('language_id', $defaultLang->id);
        }]);
        if (isset($_GET['withDegreeDetail']) && $_GET['withDegreeDetail'] == '1') {
            $degrees = $degrees->with('DegreeDetail');
        }
        return $degrees;
    }

    protected function sortingAndLimit($degrees)
    {
        $sortType = ['ASC', 'asc', 'DESC', 'desc'];
        $sortBy = ['id', 'name', 'abbreviation', 'Business category_name'];
        if (isset($_GET['sortBy']) && $_GET['sortBy'] != '' && isset($_GET['sortType']) && $_GET['sortType'] != '' && in_array($_GET['sortBy'], $sortBy) && in_array($_GET['sortType'], $sortType)) {
            $degrees = $degrees->OrderBy($_GET['sortBy'], $_GET['sortType']);
        }

        if (isset($_GET['limit']) && $_GET['limit'] != '') {
            $limit = $_GET['limit'];
        } else {
            $limit = 10;
        }

        return $degrees->paginate($limit);
    }

    protected function whereClause($degrees)
    {
        if (isset($_GET['searchParam']) && $_GET['searchParam'] != '') {
            $degrees = $degrees->whereHas('DegreeDetail', function ($q) {
                $q->where('name', 'LIKE', '%' . $_GET['searchParam'] . '%');
            });
        }
        return $degrees;
    }
}
