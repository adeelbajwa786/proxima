<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\UserResource;
use App\Models\User;
use App\Traits\StatusResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use StatusResponser;


    public function updateProfile(Request $request)
    {
        $rules = [
            'current_password' => ['required', 'string', 'max:50'],
            'new_password' => ['required', 'confirmed', 'max:10'],
        ];
        $this->validate($request, $rules);

        if (!Hash::check($request->current_password, auth()->user()->password)) {
            return $this->errorResponse("Old Password Doesn't match!");
        }

        $user = User::whereId(auth()->user()->id)->update([
            'password' => Hash::make($request->new_password)
        ]);

        if ($user) {
            return $this->successResponse([], 'Profile has been updated successfully.');
        }
        return $this->errorResponse();
    }
}
