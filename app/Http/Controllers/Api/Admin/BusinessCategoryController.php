<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\BusinessCategoryResource;
use Illuminate\Support\Str;
use App\Models\BusinessCategory;
use App\Models\BusinessCategoryDetail;
use App\Models\Language;
use App\Rules\CheckCategorySlug;
use App\Traits\StatusResponser;
use App\Traits\FileUploadTrait;
use Illuminate\Http\Request;

class BusinessCategoryController extends Controller
{
    use StatusResponser;
    use FileUploadTrait;


    public function index()
    {
        $businessCategories = BusinessCategory::query();

        $businessCategories = $this->whereClause($businessCategories);
        $businessCategories = $this->loadRelations($businessCategories);
        $businessCategories = $this->sortingAndLimit($businessCategories);

        return $this->apiSuccessResponse(BusinessCategoryResource::collection($businessCategories), 'Data Get Successfully!');
    }


    public function show(BusinessCategory $businessCategory)
    {
        if (isset($_GET['withBusinessCategoryDetail']) && $_GET['withBusinessCategoryDetail'] == '1') {
            $businessCategory = $businessCategory->loadMissing('businessCategoryDetail');
        }

        return $this->apiSuccessResponse(new BusinessCategoryResource($businessCategory), 'Data Get Successfully!');
    }


    public function store(Request $request)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );
        $businessCategory = BusinessCategory::create([]);
        foreach ($languages as $language) {
            BusinessCategoryDetail::create([
                'business_category_id' => $businessCategory->id,
                'language_id' => $language->id,
                'name' => $request['name']['name_' . $language->id],
                'slug' => Str::slug($request['name']['name_' . $language->id]),
            ]);
        }

        if ($businessCategory) {
            return $this->apiSuccessResponse(new BusinessCategoryResource($businessCategory), 'Business category has been added successfully.');
        }
        return $this->errorResponse();
    }


    public function update(Request $request, BusinessCategory $businessCategory)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, $businessCategory->id)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );


        $businessCategory->touch();
        foreach ($languages as $language) {
            $businessCategoryDetail = BusinessCategoryDetail::whereLanguageId($language->id)->whereBusinessCategoryId($businessCategory->id)->exists();
            if ($businessCategoryDetail) {
                BusinessCategoryDetail::whereLanguageId($language->id)->whereBusinessCategoryId($businessCategory->id)->update([
                    'name' => $request['name']['name_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                ]);
            } else {
                BusinessCategoryDetail::create([
                    'business_category_id' => $businessCategory->id,
                    'language_id' => $language->id,
                    'name' => $request['name']['name_' . $language->id],
                    'slug' => Str::slug($request['name']['name_' . $language->id]),
                ]);
            }
        }

        if ($businessCategory) {
            return $this->apiSuccessResponse(new BusinessCategoryResource($businessCategory), 'Business category has been updated successfully.');
        }
        return $this->errorResponse();
    }


    public function destroy(BusinessCategory $businessCategory)
    {
        if ($businessCategory->businessCategoryDetail()->delete() && $businessCategory->delete()) {
            return $this->apiSuccessResponse(new BusinessCategoryResource($businessCategory), 'Business category has been deleted successfully.');
        }
        return $this->errorResponse();
    }

    protected function loadRelations($businessCategories)
    {
        $defaultLang = getDefaultLanguage();
        $businessCategories = $businessCategories->with(['businessCategoryDetail' => function ($q) use ($defaultLang) {
            $q->where('language_id', $defaultLang->id);
        }]);
        if (isset($_GET['withBusinessCategoryDetail']) && $_GET['withBusinessCategoryDetail'] == '1') {
            $businessCategories = $businessCategories->with('businessCategoryDetail');
        }
        return $businessCategories;
    }

    protected function sortingAndLimit($businessCategories)
    {
        $sortType = ['ASC', 'asc', 'DESC', 'desc'];
        $sortBy = ['id', 'name', 'abbreviation', 'Business category_name'];
        if (isset($_GET['sortBy']) && $_GET['sortBy'] != '' && isset($_GET['sortType']) && $_GET['sortType'] != '' && in_array($_GET['sortBy'], $sortBy) && in_array($_GET['sortType'], $sortType)) {
            $businessCategories = $businessCategories->OrderBy($_GET['sortBy'], $_GET['sortType']);
        }

        if (isset($_GET['limit']) && $_GET['limit'] != '') {
            $limit = $_GET['limit'];
        } else {
            $limit = 10;
        }

        return $businessCategories->paginate($limit);
    }

    protected function whereClause($businessCategories)
    {
        if (isset($_GET['searchParam']) && $_GET['searchParam'] != '') {
            $businessCategories = $businessCategories->whereHas('businessCategoryDetail', function ($q) {
                $q->where('name', 'LIKE', '%' . $_GET['searchParam'] . '%');
            });
        }
        return $businessCategories;
    }
}
