<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Web\RegistrationPackageResource;
use App\Models\Customer;
use Illuminate\Support\Str;
use App\Models\RegistrationPackage;
use App\Models\RegistrationPackageDetail;
use App\Rules\CheckCategorySlug;
use App\Traits\StatusResponser;
use App\Traits\FileUploadTrait;
use Illuminate\Http\Request;

class RegistrationPackageController extends Controller
{
    use StatusResponser;
    use FileUploadTrait;


    public function index()
    {
        $registrationPackages = RegistrationPackage::query();

        $registrationPackages = $this->whereClause($registrationPackages);
        $registrationPackages = $this->loadRelations($registrationPackages);
        $registrationPackages = $this->sortingAndLimit($registrationPackages);

        return $this->apiSuccessResponse(RegistrationPackageResource::collection($registrationPackages), 'Data Get Successfully!');
    }


    public function show(RegistrationPackage $registrationPackage)
    {
        if (isset($_GET['withRegistrationPackageDetail']) && $_GET['withRegistrationPackageDetail'] == '1') {
            $registrationPackage = $registrationPackage->loadMissing('registrationPackageDetail');
        }

        return $this->apiSuccessResponse(new RegistrationPackageResource($registrationPackage), 'Data Get Successfully!');
    }


    public function store(Request $request)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, null)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }
        $validationRule = array_merge($validationRule, ['price' => ['nullable', 'regex:/^\d+(\.\d{1,6})?$/']]);
        $validationRule = array_merge($validationRule, ['free_subscription_days' => ['nullable', 'regex:/^\d+(\.\d{1,6})?$/']]);
        $validationRule = array_merge($validationRule, ['is_default' => ['required', 'boolean']]);

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );
        $registrationPackage = RegistrationPackage::create([
            'price' => isset($request->price) ? $request->price : 0,
            'free_subscription_days' => isset($request->free_subscription_days) ? $request->free_subscription_days : 0,
            'is_default' => $request->is_default,
        ]);
        foreach ($languages as $language) {
            RegistrationPackageDetail::create([
                'registration_package_id' => $registrationPackage->id,
                'language_id' => $language->id,
                'name' => $request['name']['name_' . $language->id],
            ]);
        }

        if ($registrationPackage) {
            if ($request->is_default == true) {
                $this->removeDefaultPackage($registrationPackage);
            }
            return $this->apiSuccessResponse(new RegistrationPackageResource($registrationPackage), 'Package has been added successfully.');
        }
        return $this->errorResponse();
    }


    public function update(Request $request, RegistrationPackage $registrationPackage)
    {
        $validationRule = [];
        $errorMessages = [];
        $languages = getAllLanguages();
        foreach ($languages as $language) {
            $validationRule = array_merge($validationRule, ['name.name_' . $language->id => ['required', 'string', new CheckCategorySlug($language, $registrationPackage->id)]]);
            $errorMessages = array_merge($errorMessages, ['name.name_' . $language->id . '.required' => 'Name in ' . $language->name . ' is required.']);
        }
        $validationRule = array_merge($validationRule, ['price' => ['nullable', 'regex:/^\d+(\.\d{1,6})?$/']]);
        $validationRule = array_merge($validationRule, ['free_subscription_days' => ['nullable', 'regex:/^\d+(\.\d{1,6})?$/']]);
        $validationRule = array_merge($validationRule, ['is_default' => ['required', 'boolean']]);

        $this->validate(
            $request,
            $validationRule,
            $errorMessages
        );


        $registrationPackage->update([
            'price' => isset($request->price) ? $request->price : 0,
            'free_subscription_days' => isset($request->free_subscription_days) ? $request->free_subscription_days : 0,
            'is_default' => $request->is_default,
        ]);
        foreach ($languages as $language) {
            $registrationPackageDetail = RegistrationPackageDetail::whereLanguageId($language->id)->whereRegistrationPackageId($registrationPackage->id)->exists();
            if ($registrationPackageDetail) {
                RegistrationPackageDetail::whereLanguageId($language->id)->whereRegistrationPackageId($registrationPackage->id)->update([
                    'name' => $request['name']['name_' . $language->id],
                ]);
            } else {
                RegistrationPackageDetail::create([
                    'registration_package_id' => $registrationPackage->id,
                    'language_id' => $language->id,
                    'name' => $request['name']['name_' . $language->id],
                ]);
            }
        }

        if ($registrationPackage) {
            if ($request->is_default == true) {
                $this->removeDefaultPackage($registrationPackage);
            }
            return $this->apiSuccessResponse(new RegistrationPackageResource($registrationPackage), 'Package has been updated successfully.');
        }
        return $this->errorResponse();
    }


    public function destroy(RegistrationPackage $registrationPackage)
    {
        $customerExists = Customer::whereRegistrationPackageId($registrationPackage->id)->exists();
        if ($customerExists) {
            return $this->errorResponse('Sorry, you can not delete this because its already used in customer.');
        }
        if ($registrationPackage->registrationPackageDetail()->delete() && $registrationPackage->delete()) {
            return $this->apiSuccessResponse(new RegistrationPackageResource($registrationPackage), 'Package has been deleted successfully.');
        }
        return $this->errorResponse();
    }

    protected function removeDefaultPackage($registrationPackage)
    {
        RegistrationPackage::where('id', '!=', $registrationPackage->id)->update([
            'is_default' => 0
        ]);
    }

    protected function loadRelations($registrationPackages)
    {
        $defaultLang = getDefaultLanguage(1);
        $registrationPackages = $registrationPackages->with(['registrationPackageDetail' => function ($q) use ($defaultLang) {
            $q->where('language_id', $defaultLang->id);
        }]);
        if (isset($_GET['withRegistrationPackageDetail']) && $_GET['withRegistrationPackageDetail'] == '1') {
            $registrationPackages = $registrationPackages->with('registrationPackageDetail');
        }
        return $registrationPackages;
    }

    protected function sortingAndLimit($registrationPackages)
    {
        $sortType = ['ASC', 'asc', 'DESC', 'desc'];
        $sortBy = ['id', 'price', 'free_subscription_days', 'is_default'];
        if (isset($_GET['sortBy']) && $_GET['sortBy'] != '' && isset($_GET['sortType']) && $_GET['sortType'] != '' && in_array($_GET['sortBy'], $sortBy) && in_array($_GET['sortType'], $sortType)) {
            $registrationPackages = $registrationPackages->OrderBy($_GET['sortBy'], $_GET['sortType']);
        }

        if (isset($_GET['limit']) && $_GET['limit'] != '') {
            $limit = $_GET['limit'];
        } else {
            $limit = 10;
        }

        return $registrationPackages->paginate($limit);
    }

    protected function whereClause($registrationPackages)
    {
        if (isset($_GET['searchParam']) && $_GET['searchParam'] != '') {
            $registrationPackages = $registrationPackages->whereHas('registrationPackageDetail', function ($q) {
                $q->where('name', 'LIKE', '%' . $_GET['searchParam'] . '%');
            });
        }
        return $registrationPackages;
    }
}
