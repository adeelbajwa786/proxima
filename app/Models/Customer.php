<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Customer extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = ['name', 'email', 'password', 'registration_package_id', 'package_price', 'free_subscription_days', 'package_subscribed_date', 'package_expiry_date', 'is_package_amount_paid'];

    protected $hidden = ['password', 'remember_token',];

    protected $casts = ['email_verified_at' => 'datetime',];

    public function registrationPackage()
    {
        return $this->belongsTo(RegistrationPackage::class);
    }

    public function customerBusinessCategory()
    {
        return $this->hasMany(CustomerBusinessCategory::class, 'customer_id', 'id');
    }

    public function customerMedia()
    {
        return $this->hasOne(CustomerMedia::class, 'customer_id', 'id');
    }

    public function customerProfile()
    {
        return $this->hasOne(CustomerProfile::class, 'customer_id', 'id');
    }

    public function customerSocialMedia()
    {
        return $this->hasOne(CustomerSocialMedia::class, 'customer_id', 'id');
    }
}
