<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    use HasFactory;

    protected $fillable = ['article_category_image'];

    public function ArticleCategoryDetail()
    {
        return $this->hasMany(ArticleCategoryDetail::class, "article_category_id", "id");
    }
    public function ArticleCategoryImage()
    {
        return $this->hasOne(Media::class, 'id', 'article_category_image');
    }
}
