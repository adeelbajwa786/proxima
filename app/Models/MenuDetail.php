<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'menu_id', 'language_id'
    ];
    public $timestamps = false;

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }
}
