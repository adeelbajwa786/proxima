<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistrationPackage extends Model
{
    use HasFactory;


    protected $fillable = ['price', 'free_subscription_days', 'is_default'];

    public function registrationPackageDetail()
    {
        return $this->hasMany(RegistrationPackageDetail::class, "registration_package_id", "id");
    }
}
