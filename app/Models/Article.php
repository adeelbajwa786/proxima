<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['article_image','article_category_id'];

    public function ArticleDetail()
    {
        return $this->hasMany(ArticleDetail::class, "article_id", "id");
    }
    public function ArticleImage()
    {
        return $this->hasOne(Media::class, 'id', 'article_image');
    }
}
